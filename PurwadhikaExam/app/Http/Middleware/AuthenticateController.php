<?php

namespace App\Http\Controllers;
use JWTAuth;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request; 
class AuthenticateController extends Controller
{
    public function login(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password'); //cuma ini yang diambil

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }

    public function getData(Request $request){
        $user = JWTAuth::parseToken()->authenticate();
    }

    public function register(Request $request){
        try{
             $user = new User;
                $user->user_name = $request->input('user_name');
                $user->first_name = $request->input('first_name');
                $user->last_name = $request->input('last_name');
                $user->email = $request->input('email');
                $user->password = bcrypt($request->input('password'));
                $user->phonenumber = $request->input('phonenumber');
                $user->address = $request->input('address');
                $user->profile_picture = $request->input('profile_picture');
                $user->save();

                $token = JWTAuth::fromUser($user);
                return response()->json(['message'=>'Succesfuly Create User','token'=> $token],200);    
        }
        catch(\Exception $e){
            return response()->json(['message'=>'Failed to create user,exception:' + $e],500);
        }
    }
}