<?php

namespace App\Http\Controllers;
 
use Illuminate\support\Facades\DB;
use Illuminate\Http\Request;
use App\UnitRumah;

class ProductController extends Controller
{
    
   public function getUnit(){

   $data = DB::table('unit')->get();

    return $data;

    } 

    public function createUnit(Request $request)
    {
        DB::beginTransaction(); 
                            
        try{
                $this->validate($request,[
                    'kavling' => 'required',
                    'blok' => 'required',
                    'no_rumah' => 'required',
                    'harga_rumah'=> 'required',
                    'luas_tanah' => 'required',
                    'luas_bangunan' => 'required',
                   'customer_id' => 'required'
                ]);

                //menggunakan cara ELOQUENT menaro data dari client tadi
                $usr = new Productlist;
                $usr->kavling = $request->kavling;
                $usr->blok =$request->blok;
                $usr->no_rumah = $request->norumah;
                $usr->harga_rumah = $request->hargarumah;
                $usr->luas_tanah = $request->luastanah;
                $usr->luas_bangunan =$request->luasbangunan;
                $usr->customer_id = $request->customer_id;
                $usr->save();



                    DB::commit();
                return "Unit has been added";

        }
        catch(\Exception $e){
                DB::rollback();
              

          return ($e->getMessage);

        }
    }
    function deleteProduct(Request $request)
    {
		DB::beginTransaction();


        try{

            $this->validate($request,[

                'id' => 'required',

            ]);

            DB::delete('delete from unit where id = ?',[$request->id]);

            DB::commit();

            return "Unit has beend deleted";

        }catch(\Exception $e){

            DB::rollBack();

            return ($e->getMessage);
        }

    } 
}

