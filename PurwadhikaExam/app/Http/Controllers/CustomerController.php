<?php

namespace App\Http\Controllers;

use Illuminate\support\Facades\DB;
use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
     function index()
    {
       

         $customers = Customer::get();
         return response()->json($customer, 200);

    }
    function addCustomer(Request $request)
    {
        DB::beginTransaction(); 
                           
        try{
                $this->validate($request,[
                    'nama' => 'required',
                    'alamat'=> 'required',
                    'nomor_telepon'=>'required',
                    'email'=>'required|email'
                    
                ]);

                //proses mengambil data dari client Ke servere
                $nama = $request->input('nama');
                $alamat = $request->input('alamat');
                $nomortelepon = $request->input('nomor_telepon');
                $email = $request->input('email');
                
                //menggunakan cara ELOQUENT menaro data dari client tadi
                $cst = new Customer;
                $cst->nama = $nama;
                $cst->alamat = $alamat;
                $cst->nomor_telepon = $nomortelepon;
                $cst->email = $email;
                $cst->save();

                    DB::commit();
                return response()->json(["message" => "Success !!"], 200);

        }
        catch(\Exception $e){
                DB::rollback();
                return response()->json(["message"=> $e->getMessage()],500);
                
        }
    }
    function deleteCustomer(Request $request)
    {
        DB::beginTransaction();
        try{
             $this->validate($request,[
                    'id' => 'required',     
                ]);

                $id = (integer)$request->input('id');
                $cst = Customer::find($id);
                
                if(empty($cst)){

                return response()->json(["message" => "User not found"],404);
                }
                
                $cst ->delete();

        DB::commit();
        return response()->json(["message" => "Deleted !!"],200);
        }
        catch(\Exception $e){
           
         DB::rollback();
        return response()->json(["message" => $e->getMessage()],500);
        };

    }
}
