import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  charset: 'UTF-8',
  'import': 'url(https://fonts.googleapis.com/css?family=Raleway:300,400,600)',
  // !
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
  // ! normalize.css v3.0.3 | MIT License | github.com/necolas/normalize.css
  'label': {
    'verticalAlign': 'baseline'
  },
  'sub': {
    'verticalAlign': 'baseline'
  },
  'sup': {
    'verticalAlign': 'baseline'
  },
  'hr': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    '': {
      'pageBreakInside': 'avoid'
    }
  },
  'img': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    '': {
      'pageBreakInside': 'avoid'
    }
  },
  'body': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'figure': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'btn-group>btn-group': {
    'float': 'left'
  },
  'btn-toolbar btn': {
    'float': 'left'
  },
  'btn-toolbar btn-group': {
    'float': 'left'
  },
  'btn-toolbar input-group': {
    'float': 'left'
  },
  'col-xs-1': {
    'float': 'left'
  },
  'col-xs-10': {
    'float': 'left'
  },
  'col-xs-11': {
    'float': 'left'
  },
  'col-xs-12': {
    'float': 'left'
  },
  'col-xs-2': {
    'float': 'left'
  },
  'col-xs-3': {
    'float': 'left'
  },
  'col-xs-4': {
    'float': 'left'
  },
  'col-xs-5': {
    'float': 'left'
  },
  'col-xs-6': {
    'float': 'left'
  },
  'col-xs-7': {
    'float': 'left'
  },
  'col-xs-8': {
    'float': 'left'
  },
  'col-xs-9': {
    'float': 'left'
  },
  'dropdown-menu': {
    'float': 'left'
  },
  'img-responsive': {
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'img-thumbnail': {
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'table': {
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'label': {
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'navbar-fixed-bottom navbar-collapse': {
    'maxHeight': [{ 'unit': 'px', 'value': 340 }],
    'max-device-width480p&&orientationl&&scap': {
      'maxHeight': [{ 'unit': 'px', 'value': 200 }]
    }
  },
  'navbar-fixed-top navbar-collapse': {
    'maxHeight': [{ 'unit': 'px', 'value': 340 }],
    'max-device-width480p&&orientationl&&scap': {
      'maxHeight': [{ 'unit': 'px', 'value': 200 }]
    }
  },
  'pre-scrollable': {
    'maxHeight': [{ 'unit': 'px', 'value': 340 }],
    'max-device-width480p&&orientationl&&scap': {
      'maxHeight': [{ 'unit': 'px', 'value': 200 }]
    }
  },
  'html': {
    'fontFamily': 'sans-serif',
    'MsTextSizeAdjust': '100%',
    'WebkitTextSizeAdjust': '100%'
  },
  'article': {
    'display': 'block'
  },
  'aside': {
    'display': 'block'
  },
  'details': {
    'display': 'block'
  },
  'figcaption': {
    'display': 'block'
  },
  'figure': {
    'display': 'block'
  },
  'footer': {
    'display': 'block'
  },
  'header': {
    'display': 'block'
  },
  'hgroup': {
    'display': 'block'
  },
  'main': {
    'display': 'block'
  },
  'menu': {
    'display': 'block'
  },
  'nav': {
    'display': 'block'
  },
  'section': {
    'display': 'block'
  },
  'summary': {
    'display': 'block'
  },
  'audio': {
    'display': 'inline-block',
    'verticalAlign': 'baseline'
  },
  'canvas': {
    'display': 'inline-block',
    'verticalAlign': 'baseline'
  },
  'progress': {
    'display': 'inline-block',
    'verticalAlign': 'baseline'
  },
  'video': {
    'display': 'inline-block',
    'verticalAlign': 'baseline'
  },
  'audio:not([controls])': {
    'display': 'none',
    'height': [{ 'unit': 'px', 'value': 0 }]
  },
  '[hidden]': {
    'display': 'none'
  },
  'template': {
    'display': 'none'
  },
  'a': {
    'backgroundColor': 'transparent'
  },
  'a:active': {
    'outline': '0'
  },
  'a:hover': {
    'outline': '0'
  },
  'b': {
    'fontWeight': '700'
  },
  'optgroup': {
    'fontWeight': '700'
  },
  'strong': {
    'fontWeight': '700'
  },
  'dfn': {
    'fontStyle': 'italic'
  },
  'h1': {
    'margin': [{ 'unit': 'em', 'value': 0.67 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'em', 'value': 0.67 }, { 'unit': 'px', 'value': 0 }]
  },
  'mark': {
    'background': '#ff0',
    'color': '#000'
  },
  'sub': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }],
    'lineHeight': [{ 'unit': 'px', 'value': 0 }],
    'position': 'relative'
  },
  'sup': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }],
    'lineHeight': [{ 'unit': 'px', 'value': 0 }],
    'position': 'relative'
  },
  'sup': {
    'top': [{ 'unit': 'em', 'value': -0.5 }]
  },
  'sub': {
    'bottom': [{ 'unit': 'em', 'value': -0.25 }]
  },
  'img': {
    'verticalAlign': 'middle'
  },
  'svg:not(:root)': {
    'overflow': 'hidden'
  },
  'hr': {
    'boxSizing': 'content-box',
    'height': [{ 'unit': 'px', 'value': 0 }]
  },
  'pre': {
    'overflow': 'auto'
  },
  'textarea': {
    'overflow': 'auto'
  },
  'code': {
    'fontSize': [{ 'unit': 'em', 'value': 1 }]
  },
  'kbd': {
    'fontSize': [{ 'unit': 'em', 'value': 1 }]
  },
  'pre': {
    'fontSize': [{ 'unit': 'em', 'value': 1 }]
  },
  'samp': {
    'fontSize': [{ 'unit': 'em', 'value': 1 }]
  },
  'button': {
    'color': 'inherit',
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'input': {
    'color': 'inherit',
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'optgroup': {
    'color': 'inherit',
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'select': {
    'color': 'inherit',
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'textarea': {
    'color': 'inherit',
    'font': [{ 'unit': 'string', 'value': 'inherit' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'button': {
    'overflow': 'visible'
  },
  'button': {
    'textTransform': 'none'
  },
  'select': {
    'textTransform': 'none'
  },
  'button': {
    'WebkitAppearance': 'button',
    'cursor': 'pointer'
  },
  'html input[type=button]': {
    'WebkitAppearance': 'button',
    'cursor': 'pointer'
  },
  'input[type=reset]': {
    'WebkitAppearance': 'button',
    'cursor': 'pointer'
  },
  'input[type=submit]': {
    'WebkitAppearance': 'button',
    'cursor': 'pointer'
  },
  'button[disabled]': {
    'cursor': 'default'
  },
  'html input[disabled]': {
    'cursor': 'default'
  },
  'button::-moz-focus-inner': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'input::-moz-focus-inner': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'input[type=checkbox]': {
    'boxSizing': 'border-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'input[type=radio]': {
    'boxSizing': 'border-box',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'input[type=number]::-webkit-inner-spin-button': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input[type=number]::-webkit-outer-spin-button': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input[type=search]::-webkit-search-cancel-button': {
    'WebkitAppearance': 'none'
  },
  'input[type=search]::-webkit-search-decoration': {
    'WebkitAppearance': 'none'
  },
  'table': {
    'borderCollapse': 'collapse',
    'borderSpacing': '0'
  },
  'td': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'th': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  // ! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css
  'btn': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-dangeractive': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-danger:active': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-defaultactive': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-default:active': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-infoactive': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-info:active': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-primaryactive': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-primary:active': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-warningactive': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn-warning:active': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btnactive': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'btn:active': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'dropdown-menu>disabled>a:focus': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'dropdown-menu>disabled>a:hover': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'form-control': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'navbar-toggle': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'open>btn-dangerdropdown-toggle': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'open>btn-defaultdropdown-toggle': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'open>btn-infodropdown-toggle': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'open>btn-primarydropdown-toggle': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'open>btn-warningdropdown-toggle': {
    'backgroundImage': 'none',
    '>w768': {
      'display': 'none'
    }
  },
  'img-thumbnail': {
    'backgroundColor': '#f5f8fa'
  },
  'body': {
    'backgroundColor': '#f5f8fa'
  },
  'fontFace': {
    'fontFamily': ''Glyphicons Halflings'',
    'src': 'url(/fonts/vendor/bootstrap-sass/bootstrap/glyphicons-halflings-regular.eot?f4769f9bdb7466be65088239c12046d1)',
    'src': 'url(/fonts/vendor/bootstrap-sass/bootstrap/glyphicons-halflings-regular.eot?f4769f9bdb7466be65088239c12046d1?#iefix) format("embedded-opentype"),url(/fonts/vendor/bootstrap-sass/bootstrap/glyphicons-halflings-regular.woff2?448c34a56d699c29117adc64c43affeb) format("woff2"),url(/fonts/vendor/bootstrap-sass/bootstrap/glyphicons-halflings-regular.woff?fa2772327f55d8198301fdb8bcfc8158) format("woff"),url(/fonts/vendor/bootstrap-sass/bootstrap/glyphicons-halflings-regular.ttf?e18bbf611f2a2e43afc071aa2f4e1512) format("truetype"),url(/fonts/vendor/bootstrap-sass/bootstrap/glyphicons-halflings-regular.svg?89889688147bd7575d6327160d64e760#glyphicons_halflingsregular) format("svg")'
  },
  'glyphicon': {
    'position': 'relative',
    'top': [{ 'unit': 'px', 'value': 1 }],
    'display': 'inline-block',
    'fontFamily': ''Glyphicons Halflings'',
    'fontStyle': 'normal',
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'WebkitFontSmoothing': 'antialiased',
    'MozOsxFontSmoothing': 'grayscale'
  },
  'glyphicon-asterisk:before': {
    'content': '"\002a"'
  },
  'glyphicon-plus:before': {
    'content': '"\002b"'
  },
  'glyphicon-eur:before': {
    'content': '"\20ac"'
  },
  'glyphicon-euro:before': {
    'content': '"\20ac"'
  },
  'glyphicon-minus:before': {
    'content': '"\2212"'
  },
  'glyphicon-cloud:before': {
    'content': '"\2601"'
  },
  'glyphicon-envelope:before': {
    'content': '"\2709"'
  },
  'glyphicon-pencil:before': {
    'content': '"\270f"'
  },
  'glyphicon-glass:before': {
    'content': '"\e001"'
  },
  'glyphicon-music:before': {
    'content': '"\e002"'
  },
  'glyphicon-search:before': {
    'content': '"\e003"'
  },
  'glyphicon-heart:before': {
    'content': '"\e005"'
  },
  'glyphicon-star:before': {
    'content': '"\e006"'
  },
  'glyphicon-star-empty:before': {
    'content': '"\e007"'
  },
  'glyphicon-user:before': {
    'content': '"\e008"'
  },
  'glyphicon-film:before': {
    'content': '"\e009"'
  },
  'glyphicon-th-large:before': {
    'content': '"\e010"'
  },
  'glyphicon-th:before': {
    'content': '"\e011"'
  },
  'glyphicon-th-list:before': {
    'content': '"\e012"'
  },
  'glyphicon-ok:before': {
    'content': '"\e013"'
  },
  'glyphicon-remove:before': {
    'content': '"\e014"'
  },
  'glyphicon-zoom-in:before': {
    'content': '"\e015"'
  },
  'glyphicon-zoom-out:before': {
    'content': '"\e016"'
  },
  'glyphicon-off:before': {
    'content': '"\e017"'
  },
  'glyphicon-signal:before': {
    'content': '"\e018"'
  },
  'glyphicon-cog:before': {
    'content': '"\e019"'
  },
  'glyphicon-trash:before': {
    'content': '"\e020"'
  },
  'glyphicon-home:before': {
    'content': '"\e021"'
  },
  'glyphicon-file:before': {
    'content': '"\e022"'
  },
  'glyphicon-time:before': {
    'content': '"\e023"'
  },
  'glyphicon-road:before': {
    'content': '"\e024"'
  },
  'glyphicon-download-alt:before': {
    'content': '"\e025"'
  },
  'glyphicon-download:before': {
    'content': '"\e026"'
  },
  'glyphicon-upload:before': {
    'content': '"\e027"'
  },
  'glyphicon-inbox:before': {
    'content': '"\e028"'
  },
  'glyphicon-play-circle:before': {
    'content': '"\e029"'
  },
  'glyphicon-repeat:before': {
    'content': '"\e030"'
  },
  'glyphicon-refresh:before': {
    'content': '"\e031"'
  },
  'glyphicon-list-alt:before': {
    'content': '"\e032"'
  },
  'glyphicon-lock:before': {
    'content': '"\e033"'
  },
  'glyphicon-flag:before': {
    'content': '"\e034"'
  },
  'glyphicon-headphones:before': {
    'content': '"\e035"'
  },
  'glyphicon-volume-off:before': {
    'content': '"\e036"'
  },
  'glyphicon-volume-down:before': {
    'content': '"\e037"'
  },
  'glyphicon-volume-up:before': {
    'content': '"\e038"'
  },
  'glyphicon-qrcode:before': {
    'content': '"\e039"'
  },
  'glyphicon-barcode:before': {
    'content': '"\e040"'
  },
  'glyphicon-tag:before': {
    'content': '"\e041"'
  },
  'glyphicon-tags:before': {
    'content': '"\e042"'
  },
  'glyphicon-book:before': {
    'content': '"\e043"'
  },
  'glyphicon-bookmark:before': {
    'content': '"\e044"'
  },
  'glyphicon-print:before': {
    'content': '"\e045"'
  },
  'glyphicon-camera:before': {
    'content': '"\e046"'
  },
  'glyphicon-font:before': {
    'content': '"\e047"'
  },
  'glyphicon-bold:before': {
    'content': '"\e048"'
  },
  'glyphicon-italic:before': {
    'content': '"\e049"'
  },
  'glyphicon-text-height:before': {
    'content': '"\e050"'
  },
  'glyphicon-text-width:before': {
    'content': '"\e051"'
  },
  'glyphicon-align-left:before': {
    'content': '"\e052"'
  },
  'glyphicon-align-center:before': {
    'content': '"\e053"'
  },
  'glyphicon-align-right:before': {
    'content': '"\e054"'
  },
  'glyphicon-align-justify:before': {
    'content': '"\e055"'
  },
  'glyphicon-list:before': {
    'content': '"\e056"'
  },
  'glyphicon-indent-left:before': {
    'content': '"\e057"'
  },
  'glyphicon-indent-right:before': {
    'content': '"\e058"'
  },
  'glyphicon-facetime-video:before': {
    'content': '"\e059"'
  },
  'glyphicon-picture:before': {
    'content': '"\e060"'
  },
  'glyphicon-map-marker:before': {
    'content': '"\e062"'
  },
  'glyphicon-adjust:before': {
    'content': '"\e063"'
  },
  'glyphicon-tint:before': {
    'content': '"\e064"'
  },
  'glyphicon-edit:before': {
    'content': '"\e065"'
  },
  'glyphicon-share:before': {
    'content': '"\e066"'
  },
  'glyphicon-check:before': {
    'content': '"\e067"'
  },
  'glyphicon-move:before': {
    'content': '"\e068"'
  },
  'glyphicon-step-backward:before': {
    'content': '"\e069"'
  },
  'glyphicon-fast-backward:before': {
    'content': '"\e070"'
  },
  'glyphicon-backward:before': {
    'content': '"\e071"'
  },
  'glyphicon-play:before': {
    'content': '"\e072"'
  },
  'glyphicon-pause:before': {
    'content': '"\e073"'
  },
  'glyphicon-stop:before': {
    'content': '"\e074"'
  },
  'glyphicon-forward:before': {
    'content': '"\e075"'
  },
  'glyphicon-fast-forward:before': {
    'content': '"\e076"'
  },
  'glyphicon-step-forward:before': {
    'content': '"\e077"'
  },
  'glyphicon-eject:before': {
    'content': '"\e078"'
  },
  'glyphicon-chevron-left:before': {
    'content': '"\e079"'
  },
  'glyphicon-chevron-right:before': {
    'content': '"\e080"'
  },
  'glyphicon-plus-sign:before': {
    'content': '"\e081"'
  },
  'glyphicon-minus-sign:before': {
    'content': '"\e082"'
  },
  'glyphicon-remove-sign:before': {
    'content': '"\e083"'
  },
  'glyphicon-ok-sign:before': {
    'content': '"\e084"'
  },
  'glyphicon-question-sign:before': {
    'content': '"\e085"'
  },
  'glyphicon-info-sign:before': {
    'content': '"\e086"'
  },
  'glyphicon-screenshot:before': {
    'content': '"\e087"'
  },
  'glyphicon-remove-circle:before': {
    'content': '"\e088"'
  },
  'glyphicon-ok-circle:before': {
    'content': '"\e089"'
  },
  'glyphicon-ban-circle:before': {
    'content': '"\e090"'
  },
  'glyphicon-arrow-left:before': {
    'content': '"\e091"'
  },
  'glyphicon-arrow-right:before': {
    'content': '"\e092"'
  },
  'glyphicon-arrow-up:before': {
    'content': '"\e093"'
  },
  'glyphicon-arrow-down:before': {
    'content': '"\e094"'
  },
  'glyphicon-share-alt:before': {
    'content': '"\e095"'
  },
  'glyphicon-resize-full:before': {
    'content': '"\e096"'
  },
  'glyphicon-resize-small:before': {
    'content': '"\e097"'
  },
  'glyphicon-exclamation-sign:before': {
    'content': '"\e101"'
  },
  'glyphicon-gift:before': {
    'content': '"\e102"'
  },
  'glyphicon-leaf:before': {
    'content': '"\e103"'
  },
  'glyphicon-fire:before': {
    'content': '"\e104"'
  },
  'glyphicon-eye-open:before': {
    'content': '"\e105"'
  },
  'glyphicon-eye-close:before': {
    'content': '"\e106"'
  },
  'glyphicon-warning-sign:before': {
    'content': '"\e107"'
  },
  'glyphicon-plane:before': {
    'content': '"\e108"'
  },
  'glyphicon-calendar:before': {
    'content': '"\e109"'
  },
  'glyphicon-random:before': {
    'content': '"\e110"'
  },
  'glyphicon-comment:before': {
    'content': '"\e111"'
  },
  'glyphicon-magnet:before': {
    'content': '"\e112"'
  },
  'glyphicon-chevron-up:before': {
    'content': '"\e113"'
  },
  'glyphicon-chevron-down:before': {
    'content': '"\e114"'
  },
  'glyphicon-retweet:before': {
    'content': '"\e115"'
  },
  'glyphicon-shopping-cart:before': {
    'content': '"\e116"'
  },
  'glyphicon-folder-close:before': {
    'content': '"\e117"'
  },
  'glyphicon-folder-open:before': {
    'content': '"\e118"'
  },
  'glyphicon-resize-vertical:before': {
    'content': '"\e119"'
  },
  'glyphicon-resize-horizontal:before': {
    'content': '"\e120"'
  },
  'glyphicon-hdd:before': {
    'content': '"\e121"'
  },
  'glyphicon-bullhorn:before': {
    'content': '"\e122"'
  },
  'glyphicon-bell:before': {
    'content': '"\e123"'
  },
  'glyphicon-certificate:before': {
    'content': '"\e124"'
  },
  'glyphicon-thumbs-up:before': {
    'content': '"\e125"'
  },
  'glyphicon-thumbs-down:before': {
    'content': '"\e126"'
  },
  'glyphicon-hand-right:before': {
    'content': '"\e127"'
  },
  'glyphicon-hand-left:before': {
    'content': '"\e128"'
  },
  'glyphicon-hand-up:before': {
    'content': '"\e129"'
  },
  'glyphicon-hand-down:before': {
    'content': '"\e130"'
  },
  'glyphicon-circle-arrow-right:before': {
    'content': '"\e131"'
  },
  'glyphicon-circle-arrow-left:before': {
    'content': '"\e132"'
  },
  'glyphicon-circle-arrow-up:before': {
    'content': '"\e133"'
  },
  'glyphicon-circle-arrow-down:before': {
    'content': '"\e134"'
  },
  'glyphicon-globe:before': {
    'content': '"\e135"'
  },
  'glyphicon-wrench:before': {
    'content': '"\e136"'
  },
  'glyphicon-tasks:before': {
    'content': '"\e137"'
  },
  'glyphicon-filter:before': {
    'content': '"\e138"'
  },
  'glyphicon-briefcase:before': {
    'content': '"\e139"'
  },
  'glyphicon-fullscreen:before': {
    'content': '"\e140"'
  },
  'glyphicon-dashboard:before': {
    'content': '"\e141"'
  },
  'glyphicon-paperclip:before': {
    'content': '"\e142"'
  },
  'glyphicon-heart-empty:before': {
    'content': '"\e143"'
  },
  'glyphicon-link:before': {
    'content': '"\e144"'
  },
  'glyphicon-phone:before': {
    'content': '"\e145"'
  },
  'glyphicon-pushpin:before': {
    'content': '"\e146"'
  },
  'glyphicon-usd:before': {
    'content': '"\e148"'
  },
  'glyphicon-gbp:before': {
    'content': '"\e149"'
  },
  'glyphicon-sort:before': {
    'content': '"\e150"'
  },
  'glyphicon-sort-by-alphabet:before': {
    'content': '"\e151"'
  },
  'glyphicon-sort-by-alphabet-alt:before': {
    'content': '"\e152"'
  },
  'glyphicon-sort-by-order:before': {
    'content': '"\e153"'
  },
  'glyphicon-sort-by-order-alt:before': {
    'content': '"\e154"'
  },
  'glyphicon-sort-by-attributes:before': {
    'content': '"\e155"'
  },
  'glyphicon-sort-by-attributes-alt:before': {
    'content': '"\e156"'
  },
  'glyphicon-unchecked:before': {
    'content': '"\e157"'
  },
  'glyphicon-expand:before': {
    'content': '"\e158"'
  },
  'glyphicon-collapse-down:before': {
    'content': '"\e159"'
  },
  'glyphicon-collapse-up:before': {
    'content': '"\e160"'
  },
  'glyphicon-log-in:before': {
    'content': '"\e161"'
  },
  'glyphicon-flash:before': {
    'content': '"\e162"'
  },
  'glyphicon-log-out:before': {
    'content': '"\e163"'
  },
  'glyphicon-new-window:before': {
    'content': '"\e164"'
  },
  'glyphicon-record:before': {
    'content': '"\e165"'
  },
  'glyphicon-save:before': {
    'content': '"\e166"'
  },
  'glyphicon-open:before': {
    'content': '"\e167"'
  },
  'glyphicon-saved:before': {
    'content': '"\e168"'
  },
  'glyphicon-import:before': {
    'content': '"\e169"'
  },
  'glyphicon-export:before': {
    'content': '"\e170"'
  },
  'glyphicon-send:before': {
    'content': '"\e171"'
  },
  'glyphicon-floppy-disk:before': {
    'content': '"\e172"'
  },
  'glyphicon-floppy-saved:before': {
    'content': '"\e173"'
  },
  'glyphicon-floppy-remove:before': {
    'content': '"\e174"'
  },
  'glyphicon-floppy-save:before': {
    'content': '"\e175"'
  },
  'glyphicon-floppy-open:before': {
    'content': '"\e176"'
  },
  'glyphicon-credit-card:before': {
    'content': '"\e177"'
  },
  'glyphicon-transfer:before': {
    'content': '"\e178"'
  },
  'glyphicon-cutlery:before': {
    'content': '"\e179"'
  },
  'glyphicon-header:before': {
    'content': '"\e180"'
  },
  'glyphicon-compressed:before': {
    'content': '"\e181"'
  },
  'glyphicon-earphone:before': {
    'content': '"\e182"'
  },
  'glyphicon-phone-alt:before': {
    'content': '"\e183"'
  },
  'glyphicon-tower:before': {
    'content': '"\e184"'
  },
  'glyphicon-stats:before': {
    'content': '"\e185"'
  },
  'glyphicon-sd-video:before': {
    'content': '"\e186"'
  },
  'glyphicon-hd-video:before': {
    'content': '"\e187"'
  },
  'glyphicon-subtitles:before': {
    'content': '"\e188"'
  },
  'glyphicon-sound-stereo:before': {
    'content': '"\e189"'
  },
  'glyphicon-sound-dolby:before': {
    'content': '"\e190"'
  },
  'glyphicon-sound-5-1:before': {
    'content': '"\e191"'
  },
  'glyphicon-sound-6-1:before': {
    'content': '"\e192"'
  },
  'glyphicon-sound-7-1:before': {
    'content': '"\e193"'
  },
  'glyphicon-copyright-mark:before': {
    'content': '"\e194"'
  },
  'glyphicon-regisration-mark:before': {
    'content': '"\e195"'
  },
  'glyphicon-cloud-download:before': {
    'content': '"\e197"'
  },
  'glyphicon-cloud-upload:before': {
    'content': '"\e198"'
  },
  'glyphicon-tree-conifer:before': {
    'content': '"\e199"'
  },
  'glyphicon-tree-deciduous:before': {
    'content': '"\e200"'
  },
  'glyphicon-cd:before': {
    'content': '"\e201"'
  },
  'glyphicon-save-file:before': {
    'content': '"\e202"'
  },
  'glyphicon-open-file:before': {
    'content': '"\e203"'
  },
  'glyphicon-level-up:before': {
    'content': '"\e204"'
  },
  'glyphicon-copy:before': {
    'content': '"\e205"'
  },
  'glyphicon-paste:before': {
    'content': '"\e206"'
  },
  'glyphicon-alert:before': {
    'content': '"\e209"'
  },
  'glyphicon-equalizer:before': {
    'content': '"\e210"'
  },
  'glyphicon-king:before': {
    'content': '"\e211"'
  },
  'glyphicon-queen:before': {
    'content': '"\e212"'
  },
  'glyphicon-pawn:before': {
    'content': '"\e213"'
  },
  'glyphicon-bishop:before': {
    'content': '"\e214"'
  },
  'glyphicon-knight:before': {
    'content': '"\e215"'
  },
  'glyphicon-baby-formula:before': {
    'content': '"\e216"'
  },
  'glyphicon-tent:before': {
    'content': '"\26fa"'
  },
  'glyphicon-blackboard:before': {
    'content': '"\e218"'
  },
  'glyphicon-bed:before': {
    'content': '"\e219"'
  },
  'glyphicon-apple:before': {
    'content': '"\f8ff"'
  },
  'glyphicon-erase:before': {
    'content': '"\e221"'
  },
  'glyphicon-hourglass:before': {
    'content': '"\231b"'
  },
  'glyphicon-lamp:before': {
    'content': '"\e223"'
  },
  'glyphicon-duplicate:before': {
    'content': '"\e224"'
  },
  'glyphicon-piggy-bank:before': {
    'content': '"\e225"'
  },
  'glyphicon-scissors:before': {
    'content': '"\e226"'
  },
  'glyphicon-bitcoin:before': {
    'content': '"\e227"'
  },
  'glyphicon-btc:before': {
    'content': '"\e227"'
  },
  'glyphicon-xbt:before': {
    'content': '"\e227"'
  },
  'glyphicon-jpy:before': {
    'content': '"\00a5"'
  },
  'glyphicon-yen:before': {
    'content': '"\00a5"'
  },
  'glyphicon-rub:before': {
    'content': '"\20bd"'
  },
  'glyphicon-ruble:before': {
    'content': '"\20bd"'
  },
  'glyphicon-scale:before': {
    'content': '"\e230"'
  },
  'glyphicon-ice-lolly:before': {
    'content': '"\e231"'
  },
  'glyphicon-ice-lolly-tasted:before': {
    'content': '"\e232"'
  },
  'glyphicon-education:before': {
    'content': '"\e233"'
  },
  'glyphicon-option-horizontal:before': {
    'content': '"\e234"'
  },
  'glyphicon-option-vertical:before': {
    'content': '"\e235"'
  },
  'glyphicon-menu-hamburger:before': {
    'content': '"\e236"'
  },
  'glyphicon-modal-window:before': {
    'content': '"\e237"'
  },
  'glyphicon-oil:before': {
    'content': '"\e238"'
  },
  'glyphicon-grain:before': {
    'content': '"\e239"'
  },
  'glyphicon-sunglasses:before': {
    'content': '"\e240"'
  },
  'glyphicon-text-size:before': {
    'content': '"\e241"'
  },
  'glyphicon-text-color:before': {
    'content': '"\e242"'
  },
  'glyphicon-text-background:before': {
    'content': '"\e243"'
  },
  'glyphicon-object-align-top:before': {
    'content': '"\e244"'
  },
  'glyphicon-object-align-bottom:before': {
    'content': '"\e245"'
  },
  'glyphicon-object-align-horizontal:before': {
    'content': '"\e246"'
  },
  'glyphicon-object-align-left:before': {
    'content': '"\e247"'
  },
  'glyphicon-object-align-vertical:before': {
    'content': '"\e248"'
  },
  'glyphicon-object-align-right:before': {
    'content': '"\e249"'
  },
  'glyphicon-triangle-right:before': {
    'content': '"\e250"'
  },
  'glyphicon-triangle-left:before': {
    'content': '"\e251"'
  },
  'glyphicon-triangle-bottom:before': {
    'content': '"\e252"'
  },
  'glyphicon-triangle-top:before': {
    'content': '"\e253"'
  },
  'glyphicon-console:before': {
    'content': '"\e254"'
  },
  'glyphicon-superscript:before': {
    'content': '"\e255"'
  },
  'glyphicon-subscript:before': {
    'content': '"\e256"'
  },
  'glyphicon-menu-left:before': {
    'content': '"\e257"'
  },
  'glyphicon-menu-right:before': {
    'content': '"\e258"'
  },
  'glyphicon-menu-down:before': {
    'content': '"\e259"'
  },
  'glyphicon-menu-up:before': {
    'content': '"\e260"'
  },
  '*': {
    'boxSizing': 'border-box'
  },
  ':after': {
    'boxSizing': 'border-box'
  },
  ':before': {
    'boxSizing': 'border-box'
  },
  'html': {
    'fontSize': [{ 'unit': 'px', 'value': 10 }],
    'WebkitTapHighlightColor': 'transparent'
  },
  'body': {
    'fontFamily': 'Raleway,sans-serif',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'color': '#636b6f'
  },
  'button': {
    'fontFamily': 'inherit',
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'input': {
    'fontFamily': 'inherit',
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'select': {
    'fontFamily': 'inherit',
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'textarea': {
    'fontFamily': 'inherit',
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'lineHeight': [{ 'unit': 'string', 'value': 'inherit' }]
  },
  'a': {
    'color': '#3097D1',
    'textDecoration': 'none'
  },
  'a:focus': {
    'color': '#216a94',
    'textDecoration': 'underline'
  },
  'a:hover': {
    'color': '#216a94',
    'textDecoration': 'underline'
  },
  'a:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'img-responsive': {
    'display': 'block',
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'img-rounded': {
    'borderRadius': '6px'
  },
  'img-thumbnail': {
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderRadius': '4px',
    'transition': 'all .2s ease-in-out',
    'display': 'inline-block',
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'img-circle': {
    'borderRadius': '50%'
  },
  'hr': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#eee' }]
  },
  'sr-only': {
    'position': 'absolute',
    'width': [{ 'unit': 'px', 'value': 1 }],
    'height': [{ 'unit': 'px', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': -1 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'hidden',
    'clip': 'rect(0,0,0,0)',
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'sr-only-focusable:active': {
    'position': 'static',
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'visible',
    'clip': 'auto'
  },
  'sr-only-focusable:focus': {
    'position': 'static',
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'visible',
    'clip': 'auto'
  },
  '[role=button]': {
    'cursor': 'pointer'
  },
  'h1': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h2': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h3': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h4': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h5': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h6': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h1': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h2': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h3': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h4': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h5': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h6': {
    'fontFamily': 'inherit',
    'fontWeight': '500',
    'lineHeight': [{ 'unit': 'px', 'value': 1.1 }],
    'color': 'inherit'
  },
  'h1 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h1 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h2 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h2 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h3 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h3 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h4 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h4 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h5 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h5 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h6 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h6 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h1 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h1 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h2 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h2 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h3 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h3 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h4 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h4 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h5 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h5 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h6 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h6 small': {
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#777'
  },
  'h1': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h2': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h3': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h1': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h2': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h3': {
    'marginTop': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h1 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h1 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h2 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h2 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h3 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h3 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h1 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h1 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h2 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h2 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h3 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h3 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.65 }]
  },
  'h4': {
    'marginTop': [{ 'unit': 'px', 'value': 11 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h5': {
    'marginTop': [{ 'unit': 'px', 'value': 11 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h6': {
    'marginTop': [{ 'unit': 'px', 'value': 11 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h4': {
    'marginTop': [{ 'unit': 'px', 'value': 11 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h5': {
    'marginTop': [{ 'unit': 'px', 'value': 11 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h6': {
    'marginTop': [{ 'unit': 'px', 'value': 11 }],
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'h4 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h4 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h5 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h5 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h6 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h6 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h4 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h4 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h5 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h5 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h6 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h6 small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'h1': {
    'fontSize': [{ 'unit': 'px', 'value': 36 }]
  },
  'h1': {
    'fontSize': [{ 'unit': 'px', 'value': 36 }]
  },
  'h2': {
    'fontSize': [{ 'unit': 'px', 'value': 30 }]
  },
  'h2': {
    'fontSize': [{ 'unit': 'px', 'value': 30 }]
  },
  'h3': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }]
  },
  'h3': {
    'fontSize': [{ 'unit': 'px', 'value': 24 }]
  },
  'h4': {
    'fontSize': [{ 'unit': 'px', 'value': 18 }]
  },
  'h4': {
    'fontSize': [{ 'unit': 'px', 'value': 18 }]
  },
  'h5': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }]
  },
  'h5': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }]
  },
  'h6': {
    'fontSize': [{ 'unit': 'px', 'value': 12 }]
  },
  'h6': {
    'fontSize': [{ 'unit': 'px', 'value': 12 }]
  },
  'p': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 11 }, { 'unit': 'px', 'value': 0 }]
  },
  'lead': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }],
    'fontWeight': '300',
    'lineHeight': [{ 'unit': 'px', 'value': 1.4 }],
    '>w768': {
      'fontSize': [{ 'unit': 'px', 'value': 21 }]
    }
  },
  'dt': {
    'fontWeight': '700'
  },
  'kbd kbd': {
    'fontWeight': '700'
  },
  'label': {
    'fontWeight': '700'
  },
  'small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.85 }]
  },
  'small': {
    'fontSize': [{ 'unit': '%V', 'value': 0.85 }]
  },
  'mark': {
    'backgroundColor': '#fcf8e3',
    'padding': [{ 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.2 }]
  },
  'mark': {
    'backgroundColor': '#fcf8e3',
    'padding': [{ 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.2 }]
  },
  'list-inline': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'listStyle': 'none'
  },
  'list-unstyled': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'listStyle': 'none'
  },
  'text-left': {
    'textAlign': 'left'
  },
  'text-right': {
    'textAlign': 'right'
  },
  'text-center': {
    'textAlign': 'center'
  },
  'text-justify': {
    'textAlign': 'justify'
  },
  'text-nowrap': {
    'whiteSpace': 'nowrap'
  },
  'text-lowercase': {
    'textTransform': 'lowercase'
  },
  'initialism': {
    'textTransform': 'uppercase'
  },
  'text-uppercase': {
    'textTransform': 'uppercase'
  },
  'text-capitalize': {
    'textTransform': 'capitalize'
  },
  'text-muted': {
    'color': '#777'
  },
  'text-primary': {
    'color': '#3097D1'
  },
  'atext-primary:focus': {
    'color': '#2579a9'
  },
  'atext-primary:hover': {
    'color': '#2579a9'
  },
  'text-success': {
    'color': '#3c763d'
  },
  'atext-success:focus': {
    'color': '#2b542c'
  },
  'atext-success:hover': {
    'color': '#2b542c'
  },
  'text-info': {
    'color': '#31708f'
  },
  'atext-info:focus': {
    'color': '#245269'
  },
  'atext-info:hover': {
    'color': '#245269'
  },
  'text-warning': {
    'color': '#8a6d3b'
  },
  'atext-warning:focus': {
    'color': '#66512c'
  },
  'atext-warning:hover': {
    'color': '#66512c'
  },
  'text-danger': {
    'color': '#a94442'
  },
  'atext-danger:focus': {
    'color': '#843534'
  },
  'atext-danger:hover': {
    'color': '#843534'
  },
  'bg-primary': {
    'color': '#fff',
    'backgroundColor': '#3097D1'
  },
  'abg-primary:focus': {
    'backgroundColor': '#2579a9'
  },
  'abg-primary:hover': {
    'backgroundColor': '#2579a9'
  },
  'bg-success': {
    'backgroundColor': '#dff0d8'
  },
  'abg-success:focus': {
    'backgroundColor': '#c1e2b3'
  },
  'abg-success:hover': {
    'backgroundColor': '#c1e2b3'
  },
  'bg-info': {
    'backgroundColor': '#d9edf7'
  },
  'abg-info:focus': {
    'backgroundColor': '#afd9ee'
  },
  'abg-info:hover': {
    'backgroundColor': '#afd9ee'
  },
  'bg-warning': {
    'backgroundColor': '#fcf8e3'
  },
  'abg-warning:focus': {
    'backgroundColor': '#f7ecb5'
  },
  'abg-warning:hover': {
    'backgroundColor': '#f7ecb5'
  },
  'bg-danger': {
    'backgroundColor': '#f2dede'
  },
  'abg-danger:focus': {
    'backgroundColor': '#e4b9b9'
  },
  'abg-danger:hover': {
    'backgroundColor': '#e4b9b9'
  },
  'pre code': {
    'backgroundColor': 'transparent'
  },
  'table': {
    'backgroundColor': 'transparent'
  },
  'page-header': {
    'paddingBottom': [{ 'unit': 'px', 'value': 10 }],
    'margin': [{ 'unit': 'px', 'value': 44 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 0 }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#eee' }]
  },
  'dl': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'ol': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'ul': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'blockquote ol:last-child': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'blockquote p:last-child': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'blockquote ul:last-child': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'ol ol': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'ol ul': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'ul ol': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'ul ul': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'address': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }]
  },
  'dl': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }]
  },
  'ol': {
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'ul': {
    'marginBottom': [{ 'unit': 'px', 'value': 11 }]
  },
  'list-inline': {
    'marginLeft': [{ 'unit': 'px', 'value': -5 }]
  },
  'list-inline>li': {
    'display': 'inline-block',
    'paddingLeft': [{ 'unit': 'px', 'value': 5 }],
    'paddingRight': [{ 'unit': 'px', 'value': 5 }]
  },
  'dd': {
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }]
  },
  'dt': {
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }]
  },
  'dd': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'dl-horizontal dd:after': {
    'content': '" "',
    'display': 'table'
  },
  'dl-horizontal dd:before': {
    'content': '" "',
    'display': 'table'
  },
  'dl-horizontal dd:after': {
    'clear': 'both'
  },
  'btn-group-vertical>btn-group:after': {
    'clear': 'both'
  },
  'btn-toolbar:after': {
    'clear': 'both'
  },
  'clearfix:after': {
    'clear': 'both'
  },
  'container-fluid:after': {
    'clear': 'both'
  },
  'container:after': {
    'clear': 'both'
  },
  'dropdown-menu>li>a': {
    'clear': 'both'
  },
  'form-horizontal form-group:after': {
    'clear': 'both'
  },
  'modal-footer:after': {
    'clear': 'both'
  },
  'modal-header:after': {
    'clear': 'both'
  },
  'navbar-collapse:after': {
    'clear': 'both'
  },
  'navbar-header:after': {
    'clear': 'both'
  },
  'navbar:after': {
    'clear': 'both'
  },
  'pager:after': {
    'clear': 'both'
  },
  'panel-body:after': {
    'clear': 'both'
  },
  'row:after': {
    'clear': 'both'
  },
  'abbr[data-original-title]': {
    'cursor': 'help',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'dotted' }, { 'unit': 'string', 'value': '#777' }]
  },
  'abbr[title]': {
    'cursor': 'help',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'dotted' }, { 'unit': 'string', 'value': '#777' }]
  },
  'initialism': {
    'fontSize': [{ 'unit': '%V', 'value': 0.9 }]
  },
  'blockquote': {
    'padding': [{ 'unit': 'px', 'value': 11 }, { 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 11 }, { 'unit': 'px', 'value': 22 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 17.5 }],
    'borderLeft': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#eee' }]
  },
  'blockquote small': {
    'display': 'block',
    'fontSize': [{ 'unit': '%V', 'value': 0.8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'color': '#777'
  },
  'blockquote footer': {
    'display': 'block',
    'fontSize': [{ 'unit': '%V', 'value': 0.8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'color': '#777'
  },
  'blockquote small': {
    'display': 'block',
    'fontSize': [{ 'unit': '%V', 'value': 0.8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'color': '#777'
  },
  'legend': {
    'color': '#333'
  },
  'pre': {
    'color': '#333'
  },
  'blockquote small:before': {
    'content': ''\2014 \00A0''
  },
  'blockquote footer:before': {
    'content': ''\2014 \00A0''
  },
  'blockquote small:before': {
    'content': ''\2014 \00A0''
  },
  'blockquote-reverse': {
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'borderRight': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#eee' }],
    'borderLeft': [{ 'unit': 'px', 'value': 0 }],
    'textAlign': 'right'
  },
  'blockquotepull-right': {
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'borderRight': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#eee' }],
    'borderLeft': [{ 'unit': 'px', 'value': 0 }],
    'textAlign': 'right'
  },
  'code': {
    'padding': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }],
    'fontSize': [{ 'unit': '%V', 'value': 0.9 }]
  },
  'kbd': {
    'padding': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 4 }],
    'fontSize': [{ 'unit': '%V', 'value': 0.9 }]
  },
  'caption': {
    'textAlign': 'left'
  },
  'th': {
    'textAlign': 'left'
  },
  'blockquote-reverse small:before': {
    'content': ''''
  },
  'blockquote-reverse footer:before': {
    'content': ''''
  },
  'blockquote-reverse small:before': {
    'content': ''''
  },
  'blockquotepull-right small:before': {
    'content': ''''
  },
  'blockquotepull-right footer:before': {
    'content': ''''
  },
  'blockquotepull-right small:before': {
    'content': ''''
  },
  'blockquote-reverse small:after': {
    'content': ''\00A0 \2014''
  },
  'blockquote-reverse footer:after': {
    'content': ''\00A0 \2014''
  },
  'blockquote-reverse small:after': {
    'content': ''\00A0 \2014''
  },
  'blockquotepull-right small:after': {
    'content': ''\00A0 \2014''
  },
  'blockquotepull-right footer:after': {
    'content': ''\00A0 \2014''
  },
  'blockquotepull-right small:after': {
    'content': ''\00A0 \2014''
  },
  'address': {
    'fontStyle': 'normal',
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }]
  },
  'code': {
    'fontFamily': 'Menlo,Monaco,Consolas,"Courier New",monospace'
  },
  'kbd': {
    'fontFamily': 'Menlo,Monaco,Consolas,"Courier New",monospace'
  },
  'pre': {
    'fontFamily': 'Menlo,Monaco,Consolas,"Courier New",monospace'
  },
  'samp': {
    'fontFamily': 'Menlo,Monaco,Consolas,"Courier New",monospace'
  },
  'code': {
    'color': '#c7254e',
    'backgroundColor': '#f9f2f4',
    'borderRadius': '4px'
  },
  'kbd': {
    'color': '#fff',
    'backgroundColor': '#333',
    'borderRadius': '3px',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.25)' }]
  },
  'kbd kbd': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': '%V', 'value': 1 }],
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'pre': {
    'display': 'block',
    'padding': [{ 'unit': 'px', 'value': 10.5 }, { 'unit': 'px', 'value': 10.5 }, { 'unit': 'px', 'value': 10.5 }, { 'unit': 'px', 'value': 10.5 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 11 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 13 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'wordBreak': 'break-all',
    'wordWrap': 'break-word',
    'backgroundColor': '#f5f5f5',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'borderRadius': '4px'
  },
  'container-fluid:after': {
    'display': 'table',
    'content': '" "'
  },
  'container-fluid:before': {
    'display': 'table',
    'content': '" "'
  },
  'container:after': {
    'display': 'table',
    'content': '" "'
  },
  'container:before': {
    'display': 'table',
    'content': '" "'
  },
  'row:after': {
    'display': 'table',
    'content': '" "'
  },
  'row:before': {
    'display': 'table',
    'content': '" "'
  },
  'container': {
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }],
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    '>w992': {
      'width': [{ 'unit': 'px', 'value': 970 }]
    },
    '>w1200': {
      'width': [{ 'unit': 'px', 'value': 1170 }]
    }
  },
  'container-fluid': {
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }],
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    '>w992': {
      'width': [{ 'unit': 'px', 'value': 970 }]
    },
    '>w1200': {
      'width': [{ 'unit': 'px', 'value': 1170 }]
    }
  },
  'pre code': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'string', 'value': 'inherit' }],
    'color': 'inherit',
    'whiteSpace': 'pre-wrap',
    'borderRadius': '0'
  },
  'container': {
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'container-fluid': {
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'pre-scrollable': {
    'overflowY': 'scroll'
  },
  'row': {
    'marginLeft': [{ 'unit': 'px', 'value': -15 }],
    'marginRight': [{ 'unit': 'px', 'value': -15 }]
  },
  'col-lg-1': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-10': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-11': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-12': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-2': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-3': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-4': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-5': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-6': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-7': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-8': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-lg-9': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-1': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-10': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-11': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-12': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-2': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-3': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-4': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-5': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-6': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-7': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-8': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-md-9': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-1': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-10': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-11': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-12': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-2': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-3': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-4': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-5': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-6': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-7': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-8': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-sm-9': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-1': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-10': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-11': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-12': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-2': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-3': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-4': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-5': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-6': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-7': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-8': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-9': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 1 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    '>w768': {
      'float': 'left'
    },
    '>w992': {
      'float': 'left'
    },
    '>w1200': {
      'float': 'left'
    }
  },
  'col-xs-1': {
    'width': [{ 'unit': '%H', 'value': 0.0833333333 }]
  },
  'col-xs-2': {
    'width': [{ 'unit': '%H', 'value': 0.16666666670000002 }]
  },
  'col-xs-3': {
    'width': [{ 'unit': '%H', 'value': 0.25 }]
  },
  'col-xs-4': {
    'width': [{ 'unit': '%H', 'value': 0.33333333330000003 }]
  },
  'col-xs-5': {
    'width': [{ 'unit': '%H', 'value': 0.41666666669999997 }]
  },
  'col-xs-6': {
    'width': [{ 'unit': '%H', 'value': 0.5 }]
  },
  'col-xs-7': {
    'width': [{ 'unit': '%H', 'value': 0.5833333333 }]
  },
  'col-xs-8': {
    'width': [{ 'unit': '%H', 'value': 0.6666666667 }]
  },
  'col-xs-9': {
    'width': [{ 'unit': '%H', 'value': 0.75 }]
  },
  'col-xs-10': {
    'width': [{ 'unit': '%H', 'value': 0.8333333333 }]
  },
  'col-xs-11': {
    'width': [{ 'unit': '%H', 'value': 0.9166666667 }]
  },
  'col-xs-12': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'col-xs-pull-0': {
    'right': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'col-xs-pull-1': {
    'right': [{ 'unit': '%H', 'value': 0.0833333333 }]
  },
  'col-xs-pull-2': {
    'right': [{ 'unit': '%H', 'value': 0.16666666670000002 }]
  },
  'col-xs-pull-3': {
    'right': [{ 'unit': '%H', 'value': 0.25 }]
  },
  'col-xs-pull-4': {
    'right': [{ 'unit': '%H', 'value': 0.33333333330000003 }]
  },
  'col-xs-pull-5': {
    'right': [{ 'unit': '%H', 'value': 0.41666666669999997 }]
  },
  'col-xs-pull-6': {
    'right': [{ 'unit': '%H', 'value': 0.5 }]
  },
  'col-xs-pull-7': {
    'right': [{ 'unit': '%H', 'value': 0.5833333333 }]
  },
  'col-xs-pull-8': {
    'right': [{ 'unit': '%H', 'value': 0.6666666667 }]
  },
  'col-xs-pull-9': {
    'right': [{ 'unit': '%H', 'value': 0.75 }]
  },
  'col-xs-pull-10': {
    'right': [{ 'unit': '%H', 'value': 0.8333333333 }]
  },
  'col-xs-pull-11': {
    'right': [{ 'unit': '%H', 'value': 0.9166666667 }]
  },
  'col-xs-pull-12': {
    'right': [{ 'unit': '%H', 'value': 1 }]
  },
  'col-xs-push-0': {
    'left': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'col-xs-push-1': {
    'left': [{ 'unit': '%H', 'value': 0.0833333333 }]
  },
  'col-xs-push-2': {
    'left': [{ 'unit': '%H', 'value': 0.16666666670000002 }]
  },
  'col-xs-push-3': {
    'left': [{ 'unit': '%H', 'value': 0.25 }]
  },
  'col-xs-push-4': {
    'left': [{ 'unit': '%H', 'value': 0.33333333330000003 }]
  },
  'col-xs-push-5': {
    'left': [{ 'unit': '%H', 'value': 0.41666666669999997 }]
  },
  'col-xs-push-6': {
    'left': [{ 'unit': '%H', 'value': 0.5 }]
  },
  'col-xs-push-7': {
    'left': [{ 'unit': '%H', 'value': 0.5833333333 }]
  },
  'col-xs-push-8': {
    'left': [{ 'unit': '%H', 'value': 0.6666666667 }]
  },
  'col-xs-push-9': {
    'left': [{ 'unit': '%H', 'value': 0.75 }]
  },
  'col-xs-push-10': {
    'left': [{ 'unit': '%H', 'value': 0.8333333333 }]
  },
  'col-xs-push-11': {
    'left': [{ 'unit': '%H', 'value': 0.9166666667 }]
  },
  'col-xs-push-12': {
    'left': [{ 'unit': '%H', 'value': 1 }]
  },
  'col-xs-offset-0': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'col-xs-offset-1': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.0833333333 }]
  },
  'col-xs-offset-2': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.16666666670000002 }]
  },
  'col-xs-offset-3': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.25 }]
  },
  'col-xs-offset-4': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.33333333330000003 }]
  },
  'col-xs-offset-5': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.41666666669999997 }]
  },
  'col-xs-offset-6': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.5 }]
  },
  'col-xs-offset-7': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.5833333333 }]
  },
  'col-xs-offset-8': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.6666666667 }]
  },
  'col-xs-offset-9': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.75 }]
  },
  'col-xs-offset-10': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.8333333333 }]
  },
  'col-xs-offset-11': {
    'marginLeft': [{ 'unit': '%H', 'value': 0.9166666667 }]
  },
  'col-xs-offset-12': {
    'marginLeft': [{ 'unit': '%H', 'value': 1 }]
  },
  'caption': {
    'paddingTop': [{ 'unit': 'px', 'value': 8 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 8 }],
    'color': '#777'
  },
  'table': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'marginBottom': [{ 'unit': 'px', 'value': 22 }]
  },
  'table>tbody>tr>td': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'verticalAlign': 'top',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>tbody>tr>th': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'verticalAlign': 'top',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>tfoot>tr>td': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'verticalAlign': 'top',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>tfoot>tr>th': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'verticalAlign': 'top',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>thead>tr>td': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'verticalAlign': 'top',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>thead>tr>th': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 8 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'verticalAlign': 'top',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>thead>tr>th': {
    'verticalAlign': 'bottom',
    'borderBottom': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table>caption+thead>tr:first-child>td': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'table>caption+thead>tr:first-child>th': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'table>colgroup+thead>tr:first-child>td': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'table>colgroup+thead>tr:first-child>th': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'table>thead:first-child>tr:first-child>td': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'table>thead:first-child>tr:first-child>th': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'table>tbody+tbody': {
    'borderTop': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table table': {
    'backgroundColor': '#f5f8fa'
  },
  'table-condensed>tbody>tr>td': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-condensed>tbody>tr>th': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-condensed>tfoot>tr>td': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-condensed>tfoot>tr>th': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-condensed>thead>tr>td': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-condensed>thead>tr>th': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 5 }]
  },
  'table-bordered': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>tbody>tr>td': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>tbody>tr>th': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>tfoot>tr>td': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>tfoot>tr>th': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>thead>tr>td': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>thead>tr>th': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'table-bordered>thead>tr>td': {
    'borderBottomWidth': [{ 'unit': 'px', 'value': 2 }]
  },
  'table-bordered>thead>tr>th': {
    'borderBottomWidth': [{ 'unit': 'px', 'value': 2 }]
  },
  'table-striped>tbody>tr:nth-of-type(odd)': {
    'backgroundColor': '#f9f9f9'
  },
  'table-hover>tbody>tr:hover': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tbody>tractive>td': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tbody>tractive>th': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tbody>tr>tdactive': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tbody>tr>thactive': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tfoot>tractive>td': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tfoot>tractive>th': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tfoot>tr>tdactive': {
    'backgroundColor': '#f5f5f5'
  },
  'table>tfoot>tr>thactive': {
    'backgroundColor': '#f5f5f5'
  },
  'table>thead>tractive>td': {
    'backgroundColor': '#f5f5f5'
  },
  'table>thead>tractive>th': {
    'backgroundColor': '#f5f5f5'
  },
  'table>thead>tr>tdactive': {
    'backgroundColor': '#f5f5f5'
  },
  'table>thead>tr>thactive': {
    'backgroundColor': '#f5f5f5'
  },
  'table col[class*=col-]': {
    'position': 'static',
    'float': 'none',
    'display': 'table-column'
  },
  'table td[class*=col-]': {
    'position': 'static',
    'float': 'none',
    'display': 'table-cell'
  },
  'table th[class*=col-]': {
    'position': 'static',
    'float': 'none',
    'display': 'table-cell'
  },
  'table-hover>tbody>tractive:hover>td': {
    'backgroundColor': '#e8e8e8'
  },
  'table-hover>tbody>tractive:hover>th': {
    'backgroundColor': '#e8e8e8'
  },
  'table-hover>tbody>tr:hover>active': {
    'backgroundColor': '#e8e8e8'
  },
  'table-hover>tbody>tr>tdactive:hover': {
    'backgroundColor': '#e8e8e8'
  },
  'table-hover>tbody>tr>thactive:hover': {
    'backgroundColor': '#e8e8e8'
  },
  'table>tbody>trsuccess>td': {
    'backgroundColor': '#dff0d8'
  },
  'table>tbody>trsuccess>th': {
    'backgroundColor': '#dff0d8'
  },
  'table>tbody>tr>tdsuccess': {
    'backgroundColor': '#dff0d8'
  },
  'table>tbody>tr>thsuccess': {
    'backgroundColor': '#dff0d8'
  },
  'table>tfoot>trsuccess>td': {
    'backgroundColor': '#dff0d8'
  },
  'table>tfoot>trsuccess>th': {
    'backgroundColor': '#dff0d8'
  },
  'table>tfoot>tr>tdsuccess': {
    'backgroundColor': '#dff0d8'
  },
  'table>tfoot>tr>thsuccess': {
    'backgroundColor': '#dff0d8'
  },
  'table>thead>trsuccess>td': {
    'backgroundColor': '#dff0d8'
  },
  'table>thead>trsuccess>th': {
    'backgroundColor': '#dff0d8'
  },
  'table>thead>tr>tdsuccess': {
    'backgroundColor': '#dff0d8'
  },
  'table>thead>tr>thsuccess': {
    'backgroundColor': '#dff0d8'
  },
  'table-hover>tbody>trsuccess:hover>td': {
    'backgroundColor': '#d0e9c6'
  },
  'table-hover>tbody>trsuccess:hover>th': {
    'backgroundColor': '#d0e9c6'
  },
  'table-hover>tbody>tr:hover>success': {
    'backgroundColor': '#d0e9c6'
  },
  'table-hover>tbody>tr>tdsuccess:hover': {
    'backgroundColor': '#d0e9c6'
  },
  'table-hover>tbody>tr>thsuccess:hover': {
    'backgroundColor': '#d0e9c6'
  },
  'table>tbody>trinfo>td': {
    'backgroundColor': '#d9edf7'
  },
  'table>tbody>trinfo>th': {
    'backgroundColor': '#d9edf7'
  },
  'table>tbody>tr>tdinfo': {
    'backgroundColor': '#d9edf7'
  },
  'table>tbody>tr>thinfo': {
    'backgroundColor': '#d9edf7'
  },
  'table>tfoot>trinfo>td': {
    'backgroundColor': '#d9edf7'
  },
  'table>tfoot>trinfo>th': {
    'backgroundColor': '#d9edf7'
  },
  'table>tfoot>tr>tdinfo': {
    'backgroundColor': '#d9edf7'
  },
  'table>tfoot>tr>thinfo': {
    'backgroundColor': '#d9edf7'
  },
  'table>thead>trinfo>td': {
    'backgroundColor': '#d9edf7'
  },
  'table>thead>trinfo>th': {
    'backgroundColor': '#d9edf7'
  },
  'table>thead>tr>tdinfo': {
    'backgroundColor': '#d9edf7'
  },
  'table>thead>tr>thinfo': {
    'backgroundColor': '#d9edf7'
  },
  'table-hover>tbody>trinfo:hover>td': {
    'backgroundColor': '#c4e3f3'
  },
  'table-hover>tbody>trinfo:hover>th': {
    'backgroundColor': '#c4e3f3'
  },
  'table-hover>tbody>tr:hover>info': {
    'backgroundColor': '#c4e3f3'
  },
  'table-hover>tbody>tr>tdinfo:hover': {
    'backgroundColor': '#c4e3f3'
  },
  'table-hover>tbody>tr>thinfo:hover': {
    'backgroundColor': '#c4e3f3'
  },
  'table>tbody>trwarning>td': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tbody>trwarning>th': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tbody>tr>tdwarning': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tbody>tr>thwarning': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tfoot>trwarning>td': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tfoot>trwarning>th': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tfoot>tr>tdwarning': {
    'backgroundColor': '#fcf8e3'
  },
  'table>tfoot>tr>thwarning': {
    'backgroundColor': '#fcf8e3'
  },
  'table>thead>trwarning>td': {
    'backgroundColor': '#fcf8e3'
  },
  'table>thead>trwarning>th': {
    'backgroundColor': '#fcf8e3'
  },
  'table>thead>tr>tdwarning': {
    'backgroundColor': '#fcf8e3'
  },
  'table>thead>tr>thwarning': {
    'backgroundColor': '#fcf8e3'
  },
  'table-hover>tbody>trwarning:hover>td': {
    'backgroundColor': '#faf2cc'
  },
  'table-hover>tbody>trwarning:hover>th': {
    'backgroundColor': '#faf2cc'
  },
  'table-hover>tbody>tr:hover>warning': {
    'backgroundColor': '#faf2cc'
  },
  'table-hover>tbody>tr>tdwarning:hover': {
    'backgroundColor': '#faf2cc'
  },
  'table-hover>tbody>tr>thwarning:hover': {
    'backgroundColor': '#faf2cc'
  },
  'table>tbody>trdanger>td': {
    'backgroundColor': '#f2dede'
  },
  'table>tbody>trdanger>th': {
    'backgroundColor': '#f2dede'
  },
  'table>tbody>tr>tddanger': {
    'backgroundColor': '#f2dede'
  },
  'table>tbody>tr>thdanger': {
    'backgroundColor': '#f2dede'
  },
  'table>tfoot>trdanger>td': {
    'backgroundColor': '#f2dede'
  },
  'table>tfoot>trdanger>th': {
    'backgroundColor': '#f2dede'
  },
  'table>tfoot>tr>tddanger': {
    'backgroundColor': '#f2dede'
  },
  'table>tfoot>tr>thdanger': {
    'backgroundColor': '#f2dede'
  },
  'table>thead>trdanger>td': {
    'backgroundColor': '#f2dede'
  },
  'table>thead>trdanger>th': {
    'backgroundColor': '#f2dede'
  },
  'table>thead>tr>tddanger': {
    'backgroundColor': '#f2dede'
  },
  'table>thead>tr>thdanger': {
    'backgroundColor': '#f2dede'
  },
  'table-hover>tbody>trdanger:hover>td': {
    'backgroundColor': '#ebcccc'
  },
  'table-hover>tbody>trdanger:hover>th': {
    'backgroundColor': '#ebcccc'
  },
  'table-hover>tbody>tr:hover>danger': {
    'backgroundColor': '#ebcccc'
  },
  'table-hover>tbody>tr>tddanger:hover': {
    'backgroundColor': '#ebcccc'
  },
  'table-hover>tbody>tr>thdanger:hover': {
    'backgroundColor': '#ebcccc'
  },
  'table-responsive': {
    'overflowX': 'auto',
    'minHeight': [{ 'unit': '%V', 'value': 0.0001 }],
    'screen&&<w767': {
      'width': [{ 'unit': '%H', 'value': 1 }],
      'marginBottom': [{ 'unit': 'px', 'value': 16.5 }],
      'overflowY': 'hidden',
      'MsOverflowStyle': '-ms-autohiding-scrollbar',
      'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
    }
  },
  'fieldset': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'legend': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'fieldset': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'minWidth': [{ 'unit': 'px', 'value': 0 }]
  },
  'legend': {
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'fontSize': [{ 'unit': 'px', 'value': 21 }],
    'lineHeight': [{ 'unit': 'string', 'value': 'inherit' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#e5e5e5' }]
  },
  'label': {
    'display': 'inline-block',
    'marginBottom': [{ 'unit': 'px', 'value': 5 }]
  },
  'input[type=search]': {
    'boxSizing': 'border-box',
    'WebkitAppearance': 'none'
  },
  'input[type=checkbox]': {
    'margin': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'marginTop': [{ 'unit': 'px', 'value': 1 }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }]
  },
  'input[type=radio]': {
    'margin': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'marginTop': [{ 'unit': 'px', 'value': 1 }],
    'lineHeight': [{ 'unit': 'string', 'value': 'normal' }]
  },
  'form-control': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'color': '#555',
    'display': 'block'
  },
  'output': {
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'color': '#555',
    'display': 'block'
  },
  'input[type=file]': {
    'display': 'block'
  },
  'input[type=range]': {
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'select[multiple]': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'select[size]': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input[type=file]:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'input[type=checkbox]:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'input[type=radio]:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'output': {
    'paddingTop': [{ 'unit': 'px', 'value': 7 }]
  },
  'form-control': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'px', 'value': 36 }],
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccd0d2' }],
    'borderRadius': '4px',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }],
    'transition': 'border-color ease-in-out .15s,box-shadow ease-in-out .15s'
  },
  'form-control:focus': {
    'borderColor': '#98cbe8',
    'outline': '0',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075),0' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'string', 'value': 'rgba(152,203,232,.6)' }]
  },
  'form-control::-moz-placeholder': {
    'color': '#b1b7ba',
    'opacity': '1'
  },
  'form-control:-ms-input-placeholder': {
    'color': '#b1b7ba'
  },
  'form-control::-webkit-input-placeholder': {
    'color': '#b1b7ba'
  },
  'has-success checkbox': {
    'color': '#3c763d'
  },
  'has-success checkbox-inline': {
    'color': '#3c763d'
  },
  'has-success control-label': {
    'color': '#3c763d'
  },
  'has-success form-control-feedback': {
    'color': '#3c763d'
  },
  'has-success help-block': {
    'color': '#3c763d'
  },
  'has-success radio': {
    'color': '#3c763d'
  },
  'has-success radio-inline': {
    'color': '#3c763d'
  },
  'has-successcheckbox label': {
    'color': '#3c763d'
  },
  'has-successcheckbox-inline label': {
    'color': '#3c763d'
  },
  'has-successradio label': {
    'color': '#3c763d'
  },
  'has-successradio-inline label': {
    'color': '#3c763d'
  },
  'form-control::-ms-expand': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    'backgroundColor': 'transparent'
  },
  'form-control[disabled]': {
    'backgroundColor': '#eee',
    'opacity': '1'
  },
  'form-control[readonly]': {
    'backgroundColor': '#eee',
    'opacity': '1'
  },
  'fieldset[disabled] form-control': {
    'backgroundColor': '#eee',
    'opacity': '1'
  },
  'form-control[disabled]': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] form-control': {
    'cursor': 'not-allowed'
  },
  'textareaform-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group': {
    'marginBottom': [{ 'unit': 'px', 'value': 15 }]
  },
  'checkbox': {
    'position': 'relative',
    'display': 'block',
    'marginTop': [{ 'unit': 'px', 'value': 10 }],
    'marginBottom': [{ 'unit': 'px', 'value': 10 }]
  },
  'radio': {
    'position': 'relative',
    'display': 'block',
    'marginTop': [{ 'unit': 'px', 'value': 10 }],
    'marginBottom': [{ 'unit': 'px', 'value': 10 }]
  },
  'checkbox label': {
    'minHeight': [{ 'unit': 'px', 'value': 22 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 20 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'fontWeight': '400',
    'cursor': 'pointer'
  },
  'radio label': {
    'minHeight': [{ 'unit': 'px', 'value': 22 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 20 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'fontWeight': '400',
    'cursor': 'pointer'
  },
  'checkbox input[type=checkbox]': {
    'position': 'absolute',
    'marginLeft': [{ 'unit': 'px', 'value': -20 }],
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'checkbox-inline input[type=checkbox]': {
    'position': 'absolute',
    'marginLeft': [{ 'unit': 'px', 'value': -20 }],
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'radio input[type=radio]': {
    'position': 'absolute',
    'marginLeft': [{ 'unit': 'px', 'value': -20 }],
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'radio-inline input[type=radio]': {
    'position': 'absolute',
    'marginLeft': [{ 'unit': 'px', 'value': -20 }],
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'checkbox+checkbox': {
    'marginTop': [{ 'unit': 'px', 'value': -5 }]
  },
  'radio+radio': {
    'marginTop': [{ 'unit': 'px', 'value': -5 }]
  },
  'checkbox-inline': {
    'position': 'relative',
    'display': 'inline-block',
    'paddingLeft': [{ 'unit': 'px', 'value': 20 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'verticalAlign': 'middle',
    'fontWeight': '400',
    'cursor': 'pointer'
  },
  'radio-inline': {
    'position': 'relative',
    'display': 'inline-block',
    'paddingLeft': [{ 'unit': 'px', 'value': 20 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'verticalAlign': 'middle',
    'fontWeight': '400',
    'cursor': 'pointer'
  },
  'checkbox-inline+checkbox-inline': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginLeft': [{ 'unit': 'px', 'value': 10 }]
  },
  'radio-inline+radio-inline': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginLeft': [{ 'unit': 'px', 'value': 10 }]
  },
  'checkbox-inlinedisabled': {
    'cursor': 'not-allowed'
  },
  'checkboxdisabled label': {
    'cursor': 'not-allowed'
  },
  'radio-inlinedisabled': {
    'cursor': 'not-allowed'
  },
  'radiodisabled label': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] checkbox label': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] checkbox-inline': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] radio label': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] radio-inline': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] input[type=checkbox]': {
    'cursor': 'not-allowed'
  },
  'fieldset[disabled] input[type=radio]': {
    'cursor': 'not-allowed'
  },
  'input[type=checkbox]disabled': {
    'cursor': 'not-allowed'
  },
  'input[type=checkbox][disabled]': {
    'cursor': 'not-allowed'
  },
  'input[type=radio]disabled': {
    'cursor': 'not-allowed'
  },
  'input[type=radio][disabled]': {
    'cursor': 'not-allowed'
  },
  'form-control-static': {
    'paddingTop': [{ 'unit': 'px', 'value': 7 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 7 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'minHeight': [{ 'unit': 'px', 'value': 36 }]
  },
  'form-control-staticinput-lg': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'form-control-staticinput-sm': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-lg>form-control-staticform-control': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-lg>form-control-staticinput-group-addon': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-lg>input-group-btn>form-control-staticbtn': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-sm>form-control-staticform-control': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-sm>form-control-staticinput-group-addon': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-sm>input-group-btn>form-control-staticbtn': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-sm>form-control': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'input-group-sm>input-group-addon': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'input-group-sm>input-group-btn>btn': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'input-sm': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'input-group-sm>input-group-btn>selectbtn': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm>selectform-control': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm>selectinput-group-addon': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'selectinput-sm': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm>input-group-btn>select[multiple]btn': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-sm>input-group-btn>textareabtn': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-sm>select[multiple]form-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-sm>select[multiple]input-group-addon': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-sm>textareaform-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-sm>textareainput-group-addon': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'select[multiple]input-sm': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'textareainput-sm': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group-sm form-control': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'form-group-sm selectform-control': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'form-group-sm select[multiple]form-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group-sm textareaform-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group-sm form-control-static': {
    'height': [{ 'unit': 'px', 'value': 30 }],
    'minHeight': [{ 'unit': 'px', 'value': 34 }],
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }]
  },
  'input-group-lg>form-control': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'input-group-lg>input-group-addon': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'input-group-lg>input-group-btn>btn': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'input-lg': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'input-group-lg>input-group-btn>selectbtn': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg>selectform-control': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg>selectinput-group-addon': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'selectinput-lg': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg>input-group-btn>select[multiple]btn': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-lg>input-group-btn>textareabtn': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-lg>select[multiple]form-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-lg>select[multiple]input-group-addon': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-lg>textareaform-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'input-group-lg>textareainput-group-addon': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'select[multiple]input-lg': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'textareainput-lg': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group-lg form-control': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'form-group-lg selectform-control': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'form-group-lg select[multiple]form-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group-lg textareaform-control': {
    'height': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'form-group-lg form-control-static': {
    'height': [{ 'unit': 'px', 'value': 46 }],
    'minHeight': [{ 'unit': 'px', 'value': 40 }],
    'padding': [{ 'unit': 'px', 'value': 11 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 11 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }]
  },
  'has-feedback': {
    'position': 'relative'
  },
  'has-feedback form-control': {
    'paddingRight': [{ 'unit': 'px', 'value': 45 }]
  },
  'form-control-feedback': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '2',
    'display': 'block',
    'width': [{ 'unit': 'px', 'value': 36 }],
    'height': [{ 'unit': 'px', 'value': 36 }],
    'lineHeight': [{ 'unit': 'px', 'value': 36 }],
    'textAlign': 'center',
    'pointerEvents': 'none'
  },
  'collapsing': {
    'position': 'relative'
  },
  'dropdown': {
    'position': 'relative'
  },
  'dropup': {
    'position': 'relative'
  },
  'form-group-lg form-control+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 46 }],
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 46 }],
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg>form-control+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 46 }],
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg>input-group-addon+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 46 }],
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-group-lg>input-group-btn>btn+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 46 }],
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'input-lg+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 46 }],
    'height': [{ 'unit': 'px', 'value': 46 }],
    'lineHeight': [{ 'unit': 'px', 'value': 46 }]
  },
  'form-group-sm form-control+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 30 }],
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 30 }],
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm>form-control+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 30 }],
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm>input-group-addon+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 30 }],
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-group-sm>input-group-btn>btn+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 30 }],
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'input-sm+form-control-feedback': {
    'width': [{ 'unit': 'px', 'value': 30 }],
    'height': [{ 'unit': 'px', 'value': 30 }],
    'lineHeight': [{ 'unit': 'px', 'value': 30 }]
  },
  'has-success form-control': {
    'borderColor': '#3c763d',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }]
  },
  'has-success form-control:focus': {
    'borderColor': '#2b542c',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075),0' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'string', 'value': '#67b168' }]
  },
  'has-success input-group-addon': {
    'color': '#3c763d',
    'borderColor': '#3c763d',
    'backgroundColor': '#dff0d8'
  },
  'has-warning checkbox': {
    'color': '#8a6d3b'
  },
  'has-warning checkbox-inline': {
    'color': '#8a6d3b'
  },
  'has-warning control-label': {
    'color': '#8a6d3b'
  },
  'has-warning form-control-feedback': {
    'color': '#8a6d3b'
  },
  'has-warning help-block': {
    'color': '#8a6d3b'
  },
  'has-warning radio': {
    'color': '#8a6d3b'
  },
  'has-warning radio-inline': {
    'color': '#8a6d3b'
  },
  'has-warningcheckbox label': {
    'color': '#8a6d3b'
  },
  'has-warningcheckbox-inline label': {
    'color': '#8a6d3b'
  },
  'has-warningradio label': {
    'color': '#8a6d3b'
  },
  'has-warningradio-inline label': {
    'color': '#8a6d3b'
  },
  'has-warning form-control': {
    'borderColor': '#8a6d3b',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }]
  },
  'has-warning form-control:focus': {
    'borderColor': '#66512c',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075),0' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'string', 'value': '#c0a16b' }]
  },
  'has-warning input-group-addon': {
    'color': '#8a6d3b',
    'borderColor': '#8a6d3b',
    'backgroundColor': '#fcf8e3'
  },
  'has-error checkbox': {
    'color': '#a94442'
  },
  'has-error checkbox-inline': {
    'color': '#a94442'
  },
  'has-error control-label': {
    'color': '#a94442'
  },
  'has-error form-control-feedback': {
    'color': '#a94442'
  },
  'has-error help-block': {
    'color': '#a94442'
  },
  'has-error radio': {
    'color': '#a94442'
  },
  'has-error radio-inline': {
    'color': '#a94442'
  },
  'has-errorcheckbox label': {
    'color': '#a94442'
  },
  'has-errorcheckbox-inline label': {
    'color': '#a94442'
  },
  'has-errorradio label': {
    'color': '#a94442'
  },
  'has-errorradio-inline label': {
    'color': '#a94442'
  },
  'has-error form-control': {
    'borderColor': '#a94442',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }]
  },
  'has-error form-control:focus': {
    'borderColor': '#843534',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075)' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.075),0' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'string', 'value': '#ce8483' }]
  },
  'has-error input-group-addon': {
    'color': '#a94442',
    'borderColor': '#a94442',
    'backgroundColor': '#f2dede'
  },
  'has-feedback label~form-control-feedback': {
    'top': [{ 'unit': 'px', 'value': 27 }]
  },
  'has-feedback labelsr-only~form-control-feedback': {
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'help-block': {
    'display': 'block',
    'marginTop': [{ 'unit': 'px', 'value': 5 }],
    'marginBottom': [{ 'unit': 'px', 'value': 10 }],
    'color': '#a4aaae'
  },
  'form-horizontal checkbox': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'paddingTop': [{ 'unit': 'px', 'value': 7 }]
  },
  'form-horizontal checkbox-inline': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'paddingTop': [{ 'unit': 'px', 'value': 7 }]
  },
  'form-horizontal radio': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'paddingTop': [{ 'unit': 'px', 'value': 7 }]
  },
  'form-horizontal radio-inline': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'paddingTop': [{ 'unit': 'px', 'value': 7 }]
  },
  'form-horizontal checkbox': {
    'minHeight': [{ 'unit': 'px', 'value': 29 }]
  },
  'form-horizontal radio': {
    'minHeight': [{ 'unit': 'px', 'value': 29 }]
  },
  'form-horizontal form-group': {
    'marginLeft': [{ 'unit': 'px', 'value': -15 }],
    'marginRight': [{ 'unit': 'px', 'value': -15 }]
  },
  'form-horizontal form-group:after': {
    'content': '" "',
    'display': 'table'
  },
  'form-horizontal form-group:before': {
    'content': '" "',
    'display': 'table'
  },
  'form-horizontal has-feedback form-control-feedback': {
    'right': [{ 'unit': 'px', 'value': 15 }]
  },
  'btn': {
    'display': 'inline-block',
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'fontWeight': '400',
    'textAlign': 'center',
    'verticalAlign': 'middle',
    'MsTouchAction': 'manipulation',
    'touchAction': 'manipulation',
    'cursor': 'pointer',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'whiteSpace': 'nowrap',
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'borderRadius': '4px',
    'WebkitUserSelect': 'none',
    'MozUserSelect': 'none',
    'MsUserSelect': 'none',
    'userSelect': 'none'
  },
  'btnactivefocus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'btnactive:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'btnfocus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'btn:activefocus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'btn:active:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'btn:focus': {
    'outline': '-webkit-focus-ring-color auto 5px',
    'outlineOffset': '-2px'
  },
  'btnfocus': {
    'color': '#636b6f',
    'textDecoration': 'none'
  },
  'btn:focus': {
    'color': '#636b6f',
    'textDecoration': 'none'
  },
  'btn:hover': {
    'color': '#636b6f',
    'textDecoration': 'none'
  },
  'btnactive': {
    'outline': '0',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.125)' }]
  },
  'btn:active': {
    'outline': '0',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.125)' }]
  },
  'btndisabled': {
    'cursor': 'not-allowed',
    'opacity': '.65',
    'filter': 'alpha(opacity=65)',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'btn[disabled]': {
    'cursor': 'not-allowed',
    'opacity': '.65',
    'filter': 'alpha(opacity=65)',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'fieldset[disabled] btn': {
    'cursor': 'not-allowed',
    'opacity': '.65',
    'filter': 'alpha(opacity=65)',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'abtndisabled': {
    'pointerEvents': 'none'
  },
  'fieldset[disabled] abtn': {
    'pointerEvents': 'none'
  },
  'btn-default': {
    'color': '#636b6f',
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-defaultfocus': {
    'color': '#636b6f',
    'backgroundColor': '#e6e5e5',
    'borderColor': '#8c8c8c'
  },
  'btn-default:focus': {
    'color': '#636b6f',
    'backgroundColor': '#e6e5e5',
    'borderColor': '#8c8c8c'
  },
  'btn-defaultactive': {
    'color': '#636b6f',
    'backgroundColor': '#e6e5e5',
    'borderColor': '#adadad'
  },
  'btn-default:active': {
    'color': '#636b6f',
    'backgroundColor': '#e6e5e5',
    'borderColor': '#adadad'
  },
  'btn-default:hover': {
    'color': '#636b6f',
    'backgroundColor': '#e6e5e5',
    'borderColor': '#adadad'
  },
  'open>btn-defaultdropdown-toggle': {
    'color': '#636b6f',
    'backgroundColor': '#e6e5e5',
    'borderColor': '#adadad'
  },
  'btn-defaultactivefocus': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'btn-defaultactive:focus': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'btn-defaultactive:hover': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'btn-default:activefocus': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'btn-default:active:focus': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'btn-default:active:hover': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'open>btn-defaultdropdown-togglefocus': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'open>btn-defaultdropdown-toggle:focus': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'open>btn-defaultdropdown-toggle:hover': {
    'color': '#636b6f',
    'backgroundColor': '#d4d4d4',
    'borderColor': '#8c8c8c'
  },
  'btn-defaultdisabledfocus': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-defaultdisabled:focus': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-defaultdisabled:hover': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-default[disabled]focus': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-default[disabled]:focus': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-default[disabled]:hover': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'fieldset[disabled] btn-defaultfocus': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'fieldset[disabled] btn-default:focus': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'fieldset[disabled] btn-default:hover': {
    'backgroundColor': '#fff',
    'borderColor': '#ccc'
  },
  'btn-default badge': {
    'color': '#fff',
    'backgroundColor': '#636b6f'
  },
  'btn-primary': {
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primaryfocus': {
    'color': '#fff',
    'backgroundColor': '#2579a9',
    'borderColor': '#133d55'
  },
  'btn-primary:focus': {
    'color': '#fff',
    'backgroundColor': '#2579a9',
    'borderColor': '#133d55'
  },
  'btn-primaryactive': {
    'color': '#fff',
    'backgroundColor': '#2579a9',
    'borderColor': '#1f648b'
  },
  'btn-primary:active': {
    'color': '#fff',
    'backgroundColor': '#2579a9',
    'borderColor': '#1f648b'
  },
  'btn-primary:hover': {
    'color': '#fff',
    'backgroundColor': '#2579a9',
    'borderColor': '#1f648b'
  },
  'open>btn-primarydropdown-toggle': {
    'color': '#fff',
    'backgroundColor': '#2579a9',
    'borderColor': '#1f648b'
  },
  'btn-primaryactivefocus': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'btn-primaryactive:focus': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'btn-primaryactive:hover': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'btn-primary:activefocus': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'btn-primary:active:focus': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'btn-primary:active:hover': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'open>btn-primarydropdown-togglefocus': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'open>btn-primarydropdown-toggle:focus': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'open>btn-primarydropdown-toggle:hover': {
    'color': '#fff',
    'backgroundColor': '#1f648b',
    'borderColor': '#133d55'
  },
  'btn-primarydisabledfocus': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primarydisabled:focus': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primarydisabled:hover': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primary[disabled]focus': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primary[disabled]:focus': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primary[disabled]:hover': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'fieldset[disabled] btn-primaryfocus': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'fieldset[disabled] btn-primary:focus': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'fieldset[disabled] btn-primary:hover': {
    'backgroundColor': '#3097D1',
    'borderColor': '#2a88bd'
  },
  'btn-primary badge': {
    'color': '#3097D1',
    'backgroundColor': '#fff'
  },
  'btn-success': {
    'color': '#fff',
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-successfocus': {
    'color': '#fff',
    'backgroundColor': '#20895e',
    'borderColor': '#0d3625'
  },
  'btn-success:focus': {
    'color': '#fff',
    'backgroundColor': '#20895e',
    'borderColor': '#0d3625'
  },
  'btn-successactive': {
    'color': '#fff',
    'backgroundColor': '#20895e',
    'borderColor': '#196c4b'
  },
  'btn-success:active': {
    'color': '#fff',
    'backgroundColor': '#20895e',
    'borderColor': '#196c4b'
  },
  'btn-success:hover': {
    'color': '#fff',
    'backgroundColor': '#20895e',
    'borderColor': '#196c4b'
  },
  'open>btn-successdropdown-toggle': {
    'color': '#fff',
    'backgroundColor': '#20895e',
    'borderColor': '#196c4b'
  },
  'btn-successactivefocus': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'btn-successactive:focus': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'btn-successactive:hover': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'btn-success:activefocus': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'btn-success:active:focus': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'btn-success:active:hover': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'open>btn-successdropdown-togglefocus': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'open>btn-successdropdown-toggle:focus': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'open>btn-successdropdown-toggle:hover': {
    'color': '#fff',
    'backgroundColor': '#196c4b',
    'borderColor': '#0d3625'
  },
  'btn-successactive': {
    'backgroundImage': 'none'
  },
  'btn-success:active': {
    'backgroundImage': 'none'
  },
  'open>btn-successdropdown-toggle': {
    'backgroundImage': 'none'
  },
  'btn-successdisabledfocus': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-successdisabled:focus': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-successdisabled:hover': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-success[disabled]focus': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-success[disabled]:focus': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-success[disabled]:hover': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'fieldset[disabled] btn-successfocus': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'fieldset[disabled] btn-success:focus': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'fieldset[disabled] btn-success:hover': {
    'backgroundColor': '#2ab27b',
    'borderColor': '#259d6d'
  },
  'btn-success badge': {
    'color': '#2ab27b',
    'backgroundColor': '#fff'
  },
  'btn-info': {
    'color': '#fff',
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-infofocus': {
    'color': '#fff',
    'backgroundColor': '#6b9dbb',
    'borderColor': '#3d6983'
  },
  'btn-info:focus': {
    'color': '#fff',
    'backgroundColor': '#6b9dbb',
    'borderColor': '#3d6983'
  },
  'btn-infoactive': {
    'color': '#fff',
    'backgroundColor': '#6b9dbb',
    'borderColor': '#538db0'
  },
  'btn-info:active': {
    'color': '#fff',
    'backgroundColor': '#6b9dbb',
    'borderColor': '#538db0'
  },
  'btn-info:hover': {
    'color': '#fff',
    'backgroundColor': '#6b9dbb',
    'borderColor': '#538db0'
  },
  'open>btn-infodropdown-toggle': {
    'color': '#fff',
    'backgroundColor': '#6b9dbb',
    'borderColor': '#538db0'
  },
  'btn-infoactivefocus': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'btn-infoactive:focus': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'btn-infoactive:hover': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'btn-info:activefocus': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'btn-info:active:focus': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'btn-info:active:hover': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'open>btn-infodropdown-togglefocus': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'open>btn-infodropdown-toggle:focus': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'open>btn-infodropdown-toggle:hover': {
    'color': '#fff',
    'backgroundColor': '#538db0',
    'borderColor': '#3d6983'
  },
  'btn-infodisabledfocus': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-infodisabled:focus': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-infodisabled:hover': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-info[disabled]focus': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-info[disabled]:focus': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-info[disabled]:hover': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'fieldset[disabled] btn-infofocus': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'fieldset[disabled] btn-info:focus': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'fieldset[disabled] btn-info:hover': {
    'backgroundColor': '#8eb4cb',
    'borderColor': '#7da8c3'
  },
  'btn-info badge': {
    'color': '#8eb4cb',
    'backgroundColor': '#fff'
  },
  'btn-warning': {
    'color': '#fff',
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warningfocus': {
    'color': '#fff',
    'backgroundColor': '#b6a338',
    'borderColor': '#685d20'
  },
  'btn-warning:focus': {
    'color': '#fff',
    'backgroundColor': '#b6a338',
    'borderColor': '#685d20'
  },
  'btn-warningactive': {
    'color': '#fff',
    'backgroundColor': '#b6a338',
    'borderColor': '#9b8a30'
  },
  'btn-warning:active': {
    'color': '#fff',
    'backgroundColor': '#b6a338',
    'borderColor': '#9b8a30'
  },
  'btn-warning:hover': {
    'color': '#fff',
    'backgroundColor': '#b6a338',
    'borderColor': '#9b8a30'
  },
  'open>btn-warningdropdown-toggle': {
    'color': '#fff',
    'backgroundColor': '#b6a338',
    'borderColor': '#9b8a30'
  },
  'btn-warningactivefocus': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'btn-warningactive:focus': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'btn-warningactive:hover': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'btn-warning:activefocus': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'btn-warning:active:focus': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'btn-warning:active:hover': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'open>btn-warningdropdown-togglefocus': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'open>btn-warningdropdown-toggle:focus': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'open>btn-warningdropdown-toggle:hover': {
    'color': '#fff',
    'backgroundColor': '#9b8a30',
    'borderColor': '#685d20'
  },
  'btn-warningdisabledfocus': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warningdisabled:focus': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warningdisabled:hover': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warning[disabled]focus': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warning[disabled]:focus': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warning[disabled]:hover': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'fieldset[disabled] btn-warningfocus': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'fieldset[disabled] btn-warning:focus': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'fieldset[disabled] btn-warning:hover': {
    'backgroundColor': '#cbb956',
    'borderColor': '#c5b143'
  },
  'btn-warning badge': {
    'color': '#cbb956',
    'backgroundColor': '#fff'
  },
  'btn-danger': {
    'color': '#fff',
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-dangerfocus': {
    'color': '#fff',
    'backgroundColor': '#954120',
    'borderColor': '#411c0e'
  },
  'btn-danger:focus': {
    'color': '#fff',
    'backgroundColor': '#954120',
    'borderColor': '#411c0e'
  },
  'btn-dangeractive': {
    'color': '#fff',
    'backgroundColor': '#954120',
    'borderColor': '#78341a'
  },
  'btn-danger:active': {
    'color': '#fff',
    'backgroundColor': '#954120',
    'borderColor': '#78341a'
  },
  'btn-danger:hover': {
    'color': '#fff',
    'backgroundColor': '#954120',
    'borderColor': '#78341a'
  },
  'open>btn-dangerdropdown-toggle': {
    'color': '#fff',
    'backgroundColor': '#954120',
    'borderColor': '#78341a'
  },
  'btn-dangeractivefocus': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'btn-dangeractive:focus': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'btn-dangeractive:hover': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'btn-danger:activefocus': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'btn-danger:active:focus': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'btn-danger:active:hover': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'open>btn-dangerdropdown-togglefocus': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'open>btn-dangerdropdown-toggle:focus': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'open>btn-dangerdropdown-toggle:hover': {
    'color': '#fff',
    'backgroundColor': '#78341a',
    'borderColor': '#411c0e'
  },
  'btn-dangerdisabledfocus': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-dangerdisabled:focus': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-dangerdisabled:hover': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-danger[disabled]focus': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-danger[disabled]:focus': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-danger[disabled]:hover': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'fieldset[disabled] btn-dangerfocus': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'fieldset[disabled] btn-danger:focus': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'fieldset[disabled] btn-danger:hover': {
    'backgroundColor': '#bf5329',
    'borderColor': '#aa4a24'
  },
  'btn-danger badge': {
    'color': '#bf5329',
    'backgroundColor': '#fff'
  },
  'btn-link': {
    'color': '#3097D1',
    'fontWeight': '400',
    'borderRadius': '0'
  },
  'btn-link': {
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'btn-linkactive': {
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'btn-link:active': {
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'btn-link[disabled]': {
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'fieldset[disabled] btn-link': {
    'backgroundColor': 'transparent',
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'btn-link': {
    'borderColor': 'transparent'
  },
  'btn-link:active': {
    'borderColor': 'transparent'
  },
  'btn-link:focus': {
    'borderColor': 'transparent'
  },
  'btn-link:hover': {
    'borderColor': 'transparent'
  },
  'btn-link:focus': {
    'color': '#216a94',
    'textDecoration': 'underline',
    'backgroundColor': 'transparent'
  },
  'btn-link:hover': {
    'color': '#216a94',
    'textDecoration': 'underline',
    'backgroundColor': 'transparent'
  },
  'btn-link[disabled]:focus': {
    'color': '#777',
    'textDecoration': 'none'
  },
  'btn-link[disabled]:hover': {
    'color': '#777',
    'textDecoration': 'none'
  },
  'fieldset[disabled] btn-link:focus': {
    'color': '#777',
    'textDecoration': 'none'
  },
  'fieldset[disabled] btn-link:hover': {
    'color': '#777',
    'textDecoration': 'none'
  },
  'btn-group-lg>btn': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'btn-lg': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }],
    'borderRadius': '6px'
  },
  'btn-group-sm>btn': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'btn-sm': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'btn-group-xs>btn': {
    'padding': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'btn-xs': {
    'padding': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }],
    'borderRadius': '3px'
  },
  'btn-block': {
    'display': 'block',
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'btn-block+btn-block': {
    'marginTop': [{ 'unit': 'px', 'value': 5 }]
  },
  'input[type=button]btn-block': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'input[type=reset]btn-block': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'input[type=submit]btn-block': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'fade': {
    'opacity': '0',
    'transition': 'opacity .15s linear'
  },
  'fadein': {
    'opacity': '1'
  },
  'collapse': {
    'display': 'none'
  },
  'collapsein': {
    'display': 'block'
  },
  'trcollapsein': {
    'display': 'table-row'
  },
  'tbodycollapsein': {
    'display': 'table-row-group'
  },
  'collapsing': {
    'height': [{ 'unit': 'px', 'value': 0 }],
    'overflow': 'hidden',
    'transitionProperty': 'height,visibility',
    'transitionDuration': '.35s',
    'transitionTimingFunction': 'ease'
  },
  'caret': {
    'display': 'inline-block',
    'width': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 0 }],
    'marginLeft': [{ 'unit': 'px', 'value': 2 }],
    'verticalAlign': 'middle',
    'borderTop': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'dashed' }],
    'borderTop': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid\9' }],
    'borderRight': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderLeft': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }]
  },
  'dropdown-toggle:focus': {
    'outline': '0'
  },
  'dropdown-menu': {
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 1 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '1000',
    'display': 'none',
    'minWidth': [{ 'unit': 'px', 'value': 160 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'listStyle': 'none',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'textAlign': 'left',
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.15)' }],
    'borderRadius': '4px',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.175)' }],
    'backgroundClip': 'padding-box'
  },
  'dropdown-menu-right': {
    'left': [{ 'unit': 'string', 'value': 'auto' }],
    'right': [{ 'unit': 'px', 'value': 0 }]
  },
  'dropdown-menupull-right': {
    'left': [{ 'unit': 'string', 'value': 'auto' }],
    'right': [{ 'unit': 'px', 'value': 0 }]
  },
  'dropdown-header': {
    'display': 'block',
    'padding': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 20 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'whiteSpace': 'nowrap'
  },
  'dropdown-menu>li>a': {
    'display': 'block',
    'padding': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 20 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 20 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'whiteSpace': 'nowrap'
  },
  'btn-group>btn-group:first-child:not(:last-child)>btn:last-child': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'btn-group>btn-group:first-child:not(:last-child)>dropdown-toggle': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'btn-group>btn:first-child:not(:last-child):not(dropdown-toggle)': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'btn-group>btn-group:last-child:not(:first-child)>btn:first-child': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'btn-group>btn:last-child:not(:first-child)': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'btn-group>dropdown-toggle:not(:first-child)': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'btn-group-vertical>btn:not(:first-child):not(:last-child)': {
    'borderRadius': '0'
  },
  'btn-group>btn-group:not(:first-child):not(:last-child)>btn': {
    'borderRadius': '0'
  },
  'btn-group>btn:not(:first-child):not(:last-child):not(dropdown-toggle)': {
    'borderRadius': '0'
  },
  'dropdown-menu divider': {
    'height': [{ 'unit': 'px', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'hidden',
    'backgroundColor': '#e5e5e5'
  },
  'dropdown-menu>li>a': {
    'fontWeight': '400',
    'color': '#333'
  },
  'dropdown-menu>li>a:focus': {
    'textDecoration': 'none',
    'color': '#262626',
    'backgroundColor': '#f5f5f5'
  },
  'dropdown-menu>li>a:hover': {
    'textDecoration': 'none',
    'color': '#262626',
    'backgroundColor': '#f5f5f5'
  },
  'dropdown-menu>active>a': {
    'color': '#fff',
    'textDecoration': 'none',
    'outline': '0',
    'backgroundColor': '#3097D1'
  },
  'dropdown-menu>active>a:focus': {
    'color': '#fff',
    'textDecoration': 'none',
    'outline': '0',
    'backgroundColor': '#3097D1'
  },
  'dropdown-menu>active>a:hover': {
    'color': '#fff',
    'textDecoration': 'none',
    'outline': '0',
    'backgroundColor': '#3097D1'
  },
  'dropdown-menu>disabled>a': {
    'color': '#777'
  },
  'dropdown-menu>disabled>a:focus': {
    'color': '#777'
  },
  'dropdown-menu>disabled>a:hover': {
    'color': '#777'
  },
  'dropdown-menu>disabled>a:focus': {
    'textDecoration': 'none',
    'backgroundColor': 'transparent',
    'filter': 'progid:DXImageTransform.Microsoft.gradient(enabled=false)',
    'cursor': 'not-allowed'
  },
  'dropdown-menu>disabled>a:hover': {
    'textDecoration': 'none',
    'backgroundColor': 'transparent',
    'filter': 'progid:DXImageTransform.Microsoft.gradient(enabled=false)',
    'cursor': 'not-allowed'
  },
  'open>dropdown-menu': {
    'display': 'block'
  },
  'open>a': {
    'outline': '0'
  },
  'dropdown-menu-left': {
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'dropdown-header': {
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'color': '#777'
  },
  'dropdown-backdrop': {
    'position': 'fixed',
    'left': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'top': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '990'
  },
  'pull-right>dropdown-menu': {
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'dropup caret': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }],
    'borderBottom': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'dashed' }],
    'borderBottom': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid\9' }],
    'content': '""'
  },
  'navbar-fixed-bottom dropdown caret': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }],
    'borderBottom': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'dashed' }],
    'borderBottom': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'string', 'value': 'solid\9' }],
    'content': '""'
  },
  'dropup dropdown-menu': {
    'top': [{ 'unit': 'string', 'value': 'auto' }],
    'bottom': [{ 'unit': '%V', 'value': 1 }],
    'marginBottom': [{ 'unit': 'px', 'value': 2 }]
  },
  'navbar-fixed-bottom dropdown dropdown-menu': {
    'top': [{ 'unit': 'string', 'value': 'auto' }],
    'bottom': [{ 'unit': '%V', 'value': 1 }],
    'marginBottom': [{ 'unit': 'px', 'value': 2 }]
  },
  'btn-group': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'middle'
  },
  'btn-group-vertical': {
    'position': 'relative',
    'display': 'inline-block',
    'verticalAlign': 'middle'
  },
  'btn-group-vertical>btn': {
    'position': 'relative',
    'float': 'left'
  },
  'btn-group>btn': {
    'position': 'relative',
    'float': 'left'
  },
  'btn-group-vertical>btnactive': {
    'zIndex': '2'
  },
  'btn-group-vertical>btn:active': {
    'zIndex': '2'
  },
  'btn-group-vertical>btn:focus': {
    'zIndex': '2'
  },
  'btn-group-vertical>btn:hover': {
    'zIndex': '2'
  },
  'btn-group>btnactive': {
    'zIndex': '2'
  },
  'btn-group>btn:active': {
    'zIndex': '2'
  },
  'btn-group>btn:focus': {
    'zIndex': '2'
  },
  'btn-group>btn:hover': {
    'zIndex': '2'
  },
  'btn-group btn+btn': {
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'btn-group btn+btn-group': {
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'btn-group btn-group+btn': {
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'btn-group btn-group+btn-group': {
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'btn-toolbar': {
    'marginLeft': [{ 'unit': 'px', 'value': -5 }]
  },
  'btn-toolbar:after': {
    'content': '" "',
    'display': 'table'
  },
  'btn-toolbar:before': {
    'content': '" "',
    'display': 'table'
  },
  'btn-toolbar>btn': {
    'marginLeft': [{ 'unit': 'px', 'value': 5 }]
  },
  'btn-toolbar>btn-group': {
    'marginLeft': [{ 'unit': 'px', 'value': 5 }]
  },
  'btn-toolbar>input-group': {
    'marginLeft': [{ 'unit': 'px', 'value': 5 }]
  },
  'btn caret': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'btn-group>btn:first-child': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'btn-group dropdown-toggle:active': {
    'outline': '0'
  },
  'btn-groupopen dropdown-toggle': {
    'outline': '0'
  },
  'btn-group>btn+dropdown-toggle': {
    'paddingLeft': [{ 'unit': 'px', 'value': 8 }],
    'paddingRight': [{ 'unit': 'px', 'value': 8 }]
  },
  'btn-group-lgbtn-group>btn+dropdown-toggle': {
    'paddingLeft': [{ 'unit': 'px', 'value': 12 }],
    'paddingRight': [{ 'unit': 'px', 'value': 12 }]
  },
  'btn-group>btn-lg+dropdown-toggle': {
    'paddingLeft': [{ 'unit': 'px', 'value': 12 }],
    'paddingRight': [{ 'unit': 'px', 'value': 12 }]
  },
  'btn-groupopen dropdown-toggle': {
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.125)' }]
  },
  'btn-groupopen dropdown-togglebtn-link': {
    'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'btn-group-lg>btn caret': {
    'borderWidth': '5px 5px 0'
  },
  'btn-lg caret': {
    'borderWidth': '5px 5px 0'
  },
  'dropup btn-group-lg>btn caret': {
    'borderWidth': '0 5px 5px'
  },
  'dropup btn-lg caret': {
    'borderWidth': '0 5px 5px'
  },
  'btn-group-vertical>btn': {
    'display': 'block',
    'float': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'btn-group-vertical>btn-group': {
    'display': 'block',
    'float': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'btn-group-vertical>btn-group>btn': {
    'display': 'block',
    'float': 'none',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'btn-group-vertical>btn-group:after': {
    'content': '" "',
    'display': 'table'
  },
  'btn-group-vertical>btn-group:before': {
    'content': '" "',
    'display': 'table'
  },
  'btn-group-vertical>btn-group>btn': {
    'float': 'none'
  },
  'btn-group-vertical>btn+btn': {
    'marginTop': [{ 'unit': 'px', 'value': -1 }],
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'btn-group-vertical>btn+btn-group': {
    'marginTop': [{ 'unit': 'px', 'value': -1 }],
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'btn-group-vertical>btn-group+btn': {
    'marginTop': [{ 'unit': 'px', 'value': -1 }],
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'btn-group-vertical>btn-group+btn-group': {
    'marginTop': [{ 'unit': 'px', 'value': -1 }],
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'btn-group-vertical>btn:first-child:not(:last-child)': {
    'borderRadius': '4px 4px 0 0'
  },
  'btn-group-vertical>btn:last-child:not(:first-child)': {
    'borderRadius': '0 0 4px 4px'
  },
  'btn-group-vertical>btn-group:not(:first-child):not(:last-child)>btn': {
    'borderRadius': '0'
  },
  'input-group form-control:not(:first-child):not(:last-child)': {
    'borderRadius': '0'
  },
  'input-group-addon:not(:first-child):not(:last-child)': {
    'borderRadius': '0'
  },
  'input-group-btn:not(:first-child):not(:last-child)': {
    'borderRadius': '0'
  },
  'btn-group-vertical>btn-group:first-child:not(:last-child)>btn:last-child': {
    'borderBottomRightRadius': '0',
    'borderBottomLeftRadius': '0'
  },
  'btn-group-vertical>btn-group:first-child:not(:last-child)>dropdown-toggle': {
    'borderBottomRightRadius': '0',
    'borderBottomLeftRadius': '0'
  },
  'btn-group-vertical>btn-group:last-child:not(:first-child)>btn:first-child': {
    'borderTopRightRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'btn-group-justified': {
    'display': 'table',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'tableLayout': 'fixed',
    'borderCollapse': 'separate'
  },
  'btn-group-justified>btn': {
    'float': 'none',
    'display': 'table-cell',
    'width': [{ 'unit': '%H', 'value': 0.01 }]
  },
  'btn-group-justified>btn-group': {
    'float': 'none',
    'display': 'table-cell',
    'width': [{ 'unit': '%H', 'value': 0.01 }]
  },
  'btn-group-justified>btn-group btn': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'btn-group-justified>btn-group dropdown-menu': {
    'left': [{ 'unit': 'string', 'value': 'auto' }]
  },
  '[data-toggle=buttons]>btn input[type=checkbox]': {
    'position': 'absolute',
    'clip': 'rect(0,0,0,0)',
    'pointerEvents': 'none'
  },
  '[data-toggle=buttons]>btn input[type=radio]': {
    'position': 'absolute',
    'clip': 'rect(0,0,0,0)',
    'pointerEvents': 'none'
  },
  '[data-toggle=buttons]>btn-group>btn input[type=checkbox]': {
    'position': 'absolute',
    'clip': 'rect(0,0,0,0)',
    'pointerEvents': 'none'
  },
  '[data-toggle=buttons]>btn-group>btn input[type=radio]': {
    'position': 'absolute',
    'clip': 'rect(0,0,0,0)',
    'pointerEvents': 'none'
  },
  'input-group': {
    'position': 'relative'
  },
  'input-group-btn': {
    'position': 'relative'
  },
  'input-group-btn>btn': {
    'position': 'relative'
  },
  'input-group': {
    'display': 'table',
    'borderCollapse': 'separate'
  },
  'input-group[class*=col-]': {
    'float': 'none',
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'paddingRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group form-control': {
    'position': 'relative',
    'zIndex': '2',
    'float': 'left',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group form-control:focus': {
    'zIndex': '3'
  },
  'input-group form-control': {
    'display': 'table-cell'
  },
  'input-group-addon': {
    'display': 'table-cell'
  },
  'input-group-btn': {
    'display': 'table-cell'
  },
  'input-group-addon': {
    'width': [{ 'unit': '%H', 'value': 0.01 }],
    'whiteSpace': 'nowrap',
    'verticalAlign': 'middle'
  },
  'input-group-btn': {
    'width': [{ 'unit': '%H', 'value': 0.01 }],
    'whiteSpace': 'nowrap',
    'verticalAlign': 'middle'
  },
  'input-group-addon': {
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'fontWeight': '400',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#555',
    'textAlign': 'center',
    'backgroundColor': '#eee',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccd0d2' }],
    'borderRadius': '4px'
  },
  'input-group-addoninput-sm': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'borderRadius': '3px'
  },
  'input-group-sm>input-group-addon': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'borderRadius': '3px'
  },
  'input-group-sm>input-group-btn>input-group-addonbtn': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'borderRadius': '3px'
  },
  'input-group-addoninput-lg': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'borderRadius': '6px'
  },
  'input-group-lg>input-group-addon': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'borderRadius': '6px'
  },
  'input-group-lg>input-group-btn>input-group-addonbtn': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'borderRadius': '6px'
  },
  'input-group-addon input[type=checkbox]': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-addon input[type=radio]': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group form-control:first-child': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-addon:first-child': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-btn:first-child>btn': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-btn:first-child>btn-group>btn': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-btn:first-child>dropdown-toggle': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-btn:last-child>btn-group:not(:last-child)>btn': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-btn:last-child>btn:not(:last-child):not(dropdown-toggle)': {
    'borderBottomRightRadius': '0',
    'borderTopRightRadius': '0'
  },
  'input-group-addon:first-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group form-control:last-child': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-addon:last-child': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-btn:first-child>btn-group:not(:first-child)>btn': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-btn:first-child>btn:not(:first-child)': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-btn:last-child>btn': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-btn:last-child>btn-group>btn': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-btn:last-child>dropdown-toggle': {
    'borderBottomLeftRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'input-group-addon:last-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'input-group-btn': {
    'fontSize': [{ 'unit': 'px', 'value': 0 }],
    'whiteSpace': 'nowrap'
  },
  'input-group-btn>btn+btn': {
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'input-group-btn>btn:active': {
    'zIndex': '2'
  },
  'input-group-btn>btn:focus': {
    'zIndex': '2'
  },
  'input-group-btn>btn:hover': {
    'zIndex': '2'
  },
  'input-group-btn:first-child>btn': {
    'marginRight': [{ 'unit': 'px', 'value': -1 }]
  },
  'input-group-btn:first-child>btn-group': {
    'marginRight': [{ 'unit': 'px', 'value': -1 }]
  },
  'input-group-btn:last-child>btn': {
    'zIndex': '2',
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'input-group-btn:last-child>btn-group': {
    'zIndex': '2',
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'nav': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'listStyle': 'none'
  },
  'nav:after': {
    'content': '" "',
    'display': 'table'
  },
  'nav:before': {
    'content': '" "',
    'display': 'table'
  },
  'nav>li': {
    'display': 'block',
    'position': 'relative'
  },
  'nav>li>a': {
    'display': 'block',
    'position': 'relative'
  },
  'nav:after': {
    'clear': 'both'
  },
  'nav>li>a': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }]
  },
  'nav>li>a:focus': {
    'textDecoration': 'none',
    'backgroundColor': '#eee'
  },
  'nav>li>a:hover': {
    'textDecoration': 'none',
    'backgroundColor': '#eee'
  },
  'nav>lidisabled>a': {
    'color': '#777'
  },
  'nav>lidisabled>a:focus': {
    'color': '#777',
    'textDecoration': 'none',
    'backgroundColor': 'transparent',
    'cursor': 'not-allowed'
  },
  'nav>lidisabled>a:hover': {
    'color': '#777',
    'textDecoration': 'none',
    'backgroundColor': 'transparent',
    'cursor': 'not-allowed'
  },
  'nav open>a': {
    'backgroundColor': '#eee',
    'borderColor': '#3097D1'
  },
  'nav open>a:focus': {
    'backgroundColor': '#eee',
    'borderColor': '#3097D1'
  },
  'nav open>a:hover': {
    'backgroundColor': '#eee',
    'borderColor': '#3097D1'
  },
  'nav nav-divider': {
    'height': [{ 'unit': 'px', 'value': 1 }],
    'margin': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 0 }],
    'overflow': 'hidden',
    'backgroundColor': '#e5e5e5'
  },
  'nav>li>a>img': {
    'maxWidth': [{ 'unit': 'string', 'value': 'none' }]
  },
  'nav-tabs': {
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'nav-tabs>li': {
    'float': 'left',
    'marginBottom': [{ 'unit': 'px', 'value': -1 }]
  },
  'nav-tabs>li>a': {
    'marginRight': [{ 'unit': 'px', 'value': 2 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderRadius': '4px 4px 0 0'
  },
  'nav-tabs>li>a:hover': {
    'borderColor': '#eee #eee #ddd'
  },
  'nav-tabs>liactive>a': {
    'color': '#555',
    'backgroundColor': '#f5f8fa',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderBottomColor': 'transparent',
    'cursor': 'default'
  },
  'nav-tabs>liactive>a:focus': {
    'color': '#555',
    'backgroundColor': '#f5f8fa',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderBottomColor': 'transparent',
    'cursor': 'default'
  },
  'nav-tabs>liactive>a:hover': {
    'color': '#555',
    'backgroundColor': '#f5f8fa',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderBottomColor': 'transparent',
    'cursor': 'default'
  },
  'nav-pills>li': {
    'float': 'left'
  },
  'nav-justified>li': {
    'float': 'none',
    '>w768': {
      'display': 'table-cell',
      'width': [{ 'unit': '%H', 'value': 0.01 }]
    }
  },
  'nav-stacked>li': {
    'float': 'none',
    '>w768': {
      'display': 'table-cell',
      'width': [{ 'unit': '%H', 'value': 0.01 }]
    }
  },
  'nav-tabsnav-justified>li': {
    'float': 'none',
    '>w768': {
      'display': 'table-cell',
      'width': [{ 'unit': '%H', 'value': 0.01 }]
    }
  },
  'nav-pills>li>a': {
    'borderRadius': '4px'
  },
  'nav-pills>li+li': {
    'marginLeft': [{ 'unit': 'px', 'value': 2 }]
  },
  'nav-pills>liactive>a': {
    'color': '#fff',
    'backgroundColor': '#3097D1'
  },
  'nav-pills>liactive>a:focus': {
    'color': '#fff',
    'backgroundColor': '#3097D1'
  },
  'nav-pills>liactive>a:hover': {
    'color': '#fff',
    'backgroundColor': '#3097D1'
  },
  'nav-stacked>li+li': {
    'marginTop': [{ 'unit': 'px', 'value': 2 }],
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'nav-justified': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'nav-tabsnav-justified': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'nav-justified>li>a': {
    'textAlign': 'center',
    'marginBottom': [{ 'unit': 'px', 'value': 5 }]
  },
  'nav-tabsnav-justified>li>a': {
    'textAlign': 'center',
    'marginBottom': [{ 'unit': 'px', 'value': 5 }]
  },
  'nav-justified>dropdown dropdown-menu': {
    'top': [{ 'unit': 'string', 'value': 'auto' }],
    'left': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'nav-tabs-justified': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'nav-tabsnav-justified': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'nav-tabs-justified>li>a': {
    'marginRight': [{ 'unit': 'px', 'value': 0 }],
    'borderRadius': '4px'
  },
  'nav-tabsnav-justified>li>a': {
    'marginRight': [{ 'unit': 'px', 'value': 0 }],
    'borderRadius': '4px'
  },
  'nav-tabs-justified>active>a': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'nav-tabs-justified>active>a:focus': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'nav-tabs-justified>active>a:hover': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'nav-tabsnav-justified>active>a': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'nav-tabsnav-justified>active>a:focus': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'nav-tabsnav-justified>active>a:hover': {
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'tab-content>tab-pane': {
    'display': 'none'
  },
  'tab-content>active': {
    'display': 'block'
  },
  'nav-tabs dropdown-menu': {
    'marginTop': [{ 'unit': 'px', 'value': -1 }],
    'borderTopRightRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'navbar': {
    'position': 'relative',
    'minHeight': [{ 'unit': 'px', 'value': 50 }],
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    '>w768': {
      'borderRadius': '4px'
    }
  },
  'navbar:after': {
    'content': '" "',
    'display': 'table'
  },
  'navbar:before': {
    'content': '" "',
    'display': 'table'
  },
  'navbar-header:after': {
    'content': '" "',
    'display': 'table'
  },
  'navbar-header:before': {
    'content': '" "',
    'display': 'table'
  },
  'navbar-collapse': {
    'overflowX': 'visible',
    'paddingRight': [{ 'unit': 'px', 'value': 15 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(255,255,255,.1)' }],
    'WebkitOverflowScrolling': 'touch'
  },
  'navbar-collapse:after': {
    'content': '" "',
    'display': 'table'
  },
  'navbar-collapse:before': {
    'content': '" "',
    'display': 'table'
  },
  'navbar-collapsein': {
    'overflowY': 'auto'
  },
  'embed-responsive': {
    'overflow': 'hidden'
  },
  'modal': {
    'overflow': 'hidden'
  },
  'modal-open': {
    'overflow': 'hidden'
  },
  'progress': {
    'overflow': 'hidden'
  },
  'container-fluid>navbar-collapse': {
    'marginRight': [{ 'unit': 'px', 'value': -15 }],
    'marginLeft': [{ 'unit': 'px', 'value': -15 }],
    '>w768': {
      'marginRight': [{ 'unit': 'px', 'value': 0 }],
      'marginLeft': [{ 'unit': 'px', 'value': 0 }]
    }
  },
  'container-fluid>navbar-header': {
    'marginRight': [{ 'unit': 'px', 'value': -15 }],
    'marginLeft': [{ 'unit': 'px', 'value': -15 }],
    '>w768': {
      'marginRight': [{ 'unit': 'px', 'value': 0 }],
      'marginLeft': [{ 'unit': 'px', 'value': 0 }]
    }
  },
  'container>navbar-collapse': {
    'marginRight': [{ 'unit': 'px', 'value': -15 }],
    'marginLeft': [{ 'unit': 'px', 'value': -15 }],
    '>w768': {
      'marginRight': [{ 'unit': 'px', 'value': 0 }],
      'marginLeft': [{ 'unit': 'px', 'value': 0 }]
    }
  },
  'container>navbar-header': {
    'marginRight': [{ 'unit': 'px', 'value': -15 }],
    'marginLeft': [{ 'unit': 'px', 'value': -15 }],
    '>w768': {
      'marginRight': [{ 'unit': 'px', 'value': 0 }],
      'marginLeft': [{ 'unit': 'px', 'value': 0 }]
    }
  },
  'navbar-static-top': {
    'zIndex': '1000',
    'borderWidth': '0 0 1px'
  },
  'navbar-fixed-bottom': {
    'position': 'fixed',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '1030'
  },
  'navbar-fixed-top': {
    'position': 'fixed',
    'right': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '1030'
  },
  'navbar-fixed-top': {
    'top': [{ 'unit': 'px', 'value': 0 }],
    'borderWidth': '0 0 1px'
  },
  'navbar-fixed-bottom': {
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'borderWidth': '1px 0 0'
  },
  'navbar-brand': {
    'float': 'left',
    'padding': [{ 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 15 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 22 }],
    'height': [{ 'unit': 'px', 'value': 50 }]
  },
  'navbar-brand:focus': {
    'textDecoration': 'none'
  },
  'navbar-brand:hover': {
    'textDecoration': 'none'
  },
  'navbar-brand>img': {
    'display': 'block'
  },
  'navbar-toggle': {
    'position': 'relative',
    'float': 'right',
    'marginRight': [{ 'unit': 'px', 'value': 15 }],
    'padding': [{ 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 10 }],
    'marginTop': [{ 'unit': 'px', 'value': 8 }],
    'marginBottom': [{ 'unit': 'px', 'value': 8 }],
    'backgroundColor': 'transparent',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderRadius': '4px'
  },
  'navbar-toggle:focus': {
    'outline': '0'
  },
  'navbar-toggle icon-bar': {
    'display': 'block',
    'width': [{ 'unit': 'px', 'value': 22 }],
    'height': [{ 'unit': 'px', 'value': 2 }],
    'borderRadius': '1px'
  },
  'navbar-toggle icon-bar+icon-bar': {
    'marginTop': [{ 'unit': 'px', 'value': 4 }]
  },
  'navbar-nav': {
    'margin': [{ 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': -15 }, { 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': -15 }]
  },
  'navbar-nav>li>a': {
    'paddingTop': [{ 'unit': 'px', 'value': 10 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 10 }],
    'lineHeight': [{ 'unit': 'px', 'value': 22 }]
  },
  'progress-bar-striped': {
    'backgroundImage': 'linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)'
  },
  'progress-striped progress-bar': {
    'backgroundImage': 'linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)'
  },
  'progress-striped progress-bar-danger': {
    'backgroundImage': 'linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)'
  },
  'progress-striped progress-bar-info': {
    'backgroundImage': 'linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)'
  },
  'progress-striped progress-bar-success': {
    'backgroundImage': 'linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)'
  },
  'progress-striped progress-bar-warning': {
    'backgroundImage': 'linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)'
  },
  'navbar-form': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }],
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(255,255,255,.1)' }, { 'unit': 'string', 'value': 'rgba(255,255,255,.1),0' }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(255,255,255,.1)' }],
    'margin': [{ 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': -15 }, { 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': -15 }],
    '>w768': {
      'width': [{ 'unit': 'string', 'value': 'auto' }],
      'border': [{ 'unit': 'px', 'value': 0 }],
      'marginLeft': [{ 'unit': 'px', 'value': 0 }],
      'marginRight': [{ 'unit': 'px', 'value': 0 }],
      'paddingTop': [{ 'unit': 'px', 'value': 0 }],
      'paddingBottom': [{ 'unit': 'px', 'value': 0 }],
      'boxShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
    }
  },
  'breadcrumb>li': {
    'display': 'inline-block'
  },
  'pagination': {
    'display': 'inline-block'
  },
  'btn badge': {
    'top': [{ 'unit': 'px', 'value': -1 }],
    'position': 'relative'
  },
  'btn label': {
    'top': [{ 'unit': 'px', 'value': -1 }],
    'position': 'relative'
  },
  'navbar-nav>li>dropdown-menu': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'borderTopRightRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'navbar-fixed-bottom navbar-nav>li>dropdown-menu': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'borderRadius': '4px 4px 0 0'
  },
  'navbar-btn': {
    'marginTop': [{ 'unit': 'px', 'value': 7 }],
    'marginBottom': [{ 'unit': 'px', 'value': 7 }]
  },
  'btn-group-sm>navbar-btnbtn': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }],
    'marginBottom': [{ 'unit': 'px', 'value': 10 }]
  },
  'navbar-btnbtn-sm': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }],
    'marginBottom': [{ 'unit': 'px', 'value': 10 }]
  },
  'btn-group-xs>navbar-btnbtn': {
    'marginTop': [{ 'unit': 'px', 'value': 14 }],
    'marginBottom': [{ 'unit': 'px', 'value': 14 }],
    '>w768': {
      'float': 'left',
      'marginLeft': [{ 'unit': 'px', 'value': 15 }],
      'marginRight': [{ 'unit': 'px', 'value': 15 }]
    }
  },
  'navbar-btnbtn-xs': {
    'marginTop': [{ 'unit': 'px', 'value': 14 }],
    'marginBottom': [{ 'unit': 'px', 'value': 14 }],
    '>w768': {
      'float': 'left',
      'marginLeft': [{ 'unit': 'px', 'value': 15 }],
      'marginRight': [{ 'unit': 'px', 'value': 15 }]
    }
  },
  'navbar-text': {
    'marginTop': [{ 'unit': 'px', 'value': 14 }],
    'marginBottom': [{ 'unit': 'px', 'value': 14 }],
    '>w768': {
      'float': 'left',
      'marginLeft': [{ 'unit': 'px', 'value': 15 }],
      'marginRight': [{ 'unit': 'px', 'value': 15 }]
    }
  },
  'navbar-default': {
    'backgroundColor': '#fff',
    'borderColor': '#d3e0e9'
  },
  'navbar-default navbar-brand': {
    'color': '#777'
  },
  'navbar-default navbar-brand:focus': {
    'color': '#5e5d5d',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-brand:hover': {
    'color': '#5e5d5d',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-nav>li>a': {
    'color': '#777'
  },
  'navbar-default navbar-text': {
    'color': '#777'
  },
  'navbar-default navbar-nav>li>a:focus': {
    'color': '#333',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-nav>li>a:hover': {
    'color': '#333',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-nav>active>a': {
    'color': '#555',
    'backgroundColor': '#eee'
  },
  'navbar-default navbar-nav>active>a:focus': {
    'color': '#555',
    'backgroundColor': '#eee'
  },
  'navbar-default navbar-nav>active>a:hover': {
    'color': '#555',
    'backgroundColor': '#eee'
  },
  'navbar-default navbar-nav>disabled>a': {
    'color': '#ccc',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-nav>disabled>a:focus': {
    'color': '#ccc',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-nav>disabled>a:hover': {
    'color': '#ccc',
    'backgroundColor': 'transparent'
  },
  'navbar-default navbar-toggle': {
    'borderColor': '#ddd'
  },
  'navbar-default navbar-toggle:focus': {
    'backgroundColor': '#ddd'
  },
  'navbar-default navbar-toggle:hover': {
    'backgroundColor': '#ddd'
  },
  'navbar-default navbar-toggle icon-bar': {
    'backgroundColor': '#888'
  },
  'navbar-default navbar-collapse': {
    'borderColor': '#d3e0e9'
  },
  'navbar-default navbar-form': {
    'borderColor': '#d3e0e9'
  },
  'navbar-default navbar-nav>open>a': {
    'backgroundColor': '#eee',
    'color': '#555'
  },
  'navbar-default navbar-nav>open>a:focus': {
    'backgroundColor': '#eee',
    'color': '#555'
  },
  'navbar-default navbar-nav>open>a:hover': {
    'backgroundColor': '#eee',
    'color': '#555'
  },
  'navbar-default navbar-link': {
    'color': '#777'
  },
  'navbar-default navbar-link:hover': {
    'color': '#333'
  },
  'navbar-default btn-link': {
    'color': '#777'
  },
  'navbar-default btn-link:focus': {
    'color': '#333'
  },
  'navbar-default btn-link:hover': {
    'color': '#333'
  },
  'navbar-default btn-link[disabled]:focus': {
    'color': '#ccc'
  },
  'navbar-default btn-link[disabled]:hover': {
    'color': '#ccc'
  },
  'fieldset[disabled] navbar-default btn-link:focus': {
    'color': '#ccc'
  },
  'fieldset[disabled] navbar-default btn-link:hover': {
    'color': '#ccc'
  },
  'navbar-inverse': {
    'backgroundColor': '#222',
    'borderColor': '#090909'
  },
  'navbar-inverse navbar-brand': {
    'color': '#9d9d9d'
  },
  'navbar-inverse navbar-brand:focus': {
    'color': '#fff',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-brand:hover': {
    'color': '#fff',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-nav>li>a': {
    'color': '#9d9d9d'
  },
  'navbar-inverse navbar-text': {
    'color': '#9d9d9d'
  },
  'navbar-inverse navbar-nav>li>a:focus': {
    'color': '#fff',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-nav>li>a:hover': {
    'color': '#fff',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-nav>active>a': {
    'color': '#fff',
    'backgroundColor': '#090909'
  },
  'navbar-inverse navbar-nav>active>a:focus': {
    'color': '#fff',
    'backgroundColor': '#090909'
  },
  'navbar-inverse navbar-nav>active>a:hover': {
    'color': '#fff',
    'backgroundColor': '#090909'
  },
  'navbar-inverse navbar-nav>disabled>a': {
    'color': '#444',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-nav>disabled>a:focus': {
    'color': '#444',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-nav>disabled>a:hover': {
    'color': '#444',
    'backgroundColor': 'transparent'
  },
  'navbar-inverse navbar-toggle': {
    'borderColor': '#333'
  },
  'navbar-inverse navbar-toggle:focus': {
    'backgroundColor': '#333'
  },
  'navbar-inverse navbar-toggle:hover': {
    'backgroundColor': '#333'
  },
  'navbar-inverse navbar-toggle icon-bar': {
    'backgroundColor': '#fff'
  },
  'navbar-inverse navbar-collapse': {
    'borderColor': '#101010'
  },
  'navbar-inverse navbar-form': {
    'borderColor': '#101010'
  },
  'navbar-inverse navbar-nav>open>a': {
    'backgroundColor': '#090909',
    'color': '#fff'
  },
  'navbar-inverse navbar-nav>open>a:focus': {
    'backgroundColor': '#090909',
    'color': '#fff'
  },
  'navbar-inverse navbar-nav>open>a:hover': {
    'backgroundColor': '#090909',
    'color': '#fff'
  },
  'navbar-inverse navbar-link': {
    'color': '#9d9d9d'
  },
  'navbar-inverse navbar-link:hover': {
    'color': '#fff'
  },
  'navbar-inverse btn-link': {
    'color': '#9d9d9d'
  },
  'navbar-inverse btn-link:focus': {
    'color': '#fff'
  },
  'navbar-inverse btn-link:hover': {
    'color': '#fff'
  },
  'navbar-inverse btn-link[disabled]:focus': {
    'color': '#444'
  },
  'navbar-inverse btn-link[disabled]:hover': {
    'color': '#444'
  },
  'fieldset[disabled] navbar-inverse btn-link:focus': {
    'color': '#444'
  },
  'fieldset[disabled] navbar-inverse btn-link:hover': {
    'color': '#444'
  },
  'breadcrumb': {
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 15 }],
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'listStyle': 'none',
    'backgroundColor': '#f5f5f5',
    'borderRadius': '4px'
  },
  'breadcrumb>li+li:before': {
    'content': '"/ "',
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }],
    'color': '#ccc'
  },
  'breadcrumb>active': {
    'color': '#777'
  },
  'pagination': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 0 }],
    'borderRadius': '4px'
  },
  'pagination>li': {
    'display': 'inline'
  },
  'pagination>li>a': {
    'position': 'relative',
    'float': 'left',
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'textDecoration': 'none',
    'color': '#3097D1',
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'pagination>li>span': {
    'position': 'relative',
    'float': 'left',
    'padding': [{ 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }, { 'unit': 'px', 'value': 6 }, { 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'textDecoration': 'none',
    'color': '#3097D1',
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'pagination>li:first-child>a': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomLeftRadius': '4px',
    'borderTopLeftRadius': '4px'
  },
  'pagination>li:first-child>span': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomLeftRadius': '4px',
    'borderTopLeftRadius': '4px'
  },
  'pagination>li:last-child>a': {
    'borderBottomRightRadius': '4px',
    'borderTopRightRadius': '4px'
  },
  'pagination>li:last-child>span': {
    'borderBottomRightRadius': '4px',
    'borderTopRightRadius': '4px'
  },
  'pagination>li>a:focus': {
    'zIndex': '2',
    'color': '#216a94',
    'backgroundColor': '#eee',
    'borderColor': '#ddd'
  },
  'pagination>li>a:hover': {
    'zIndex': '2',
    'color': '#216a94',
    'backgroundColor': '#eee',
    'borderColor': '#ddd'
  },
  'pagination>li>span:focus': {
    'zIndex': '2',
    'color': '#216a94',
    'backgroundColor': '#eee',
    'borderColor': '#ddd'
  },
  'pagination>li>span:hover': {
    'zIndex': '2',
    'color': '#216a94',
    'backgroundColor': '#eee',
    'borderColor': '#ddd'
  },
  'pagination>active>a': {
    'zIndex': '3',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1',
    'cursor': 'default'
  },
  'pagination>active>a:focus': {
    'zIndex': '3',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1',
    'cursor': 'default'
  },
  'pagination>active>a:hover': {
    'zIndex': '3',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1',
    'cursor': 'default'
  },
  'pagination>active>span': {
    'zIndex': '3',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1',
    'cursor': 'default'
  },
  'pagination>active>span:focus': {
    'zIndex': '3',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1',
    'cursor': 'default'
  },
  'pagination>active>span:hover': {
    'zIndex': '3',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1',
    'cursor': 'default'
  },
  'pagination>disabled>a': {
    'color': '#777',
    'backgroundColor': '#fff',
    'borderColor': '#ddd',
    'cursor': 'not-allowed'
  },
  'pagination>disabled>a:focus': {
    'color': '#777',
    'backgroundColor': '#fff',
    'borderColor': '#ddd',
    'cursor': 'not-allowed'
  },
  'pagination>disabled>a:hover': {
    'color': '#777',
    'backgroundColor': '#fff',
    'borderColor': '#ddd',
    'cursor': 'not-allowed'
  },
  'pagination>disabled>span': {
    'color': '#777',
    'backgroundColor': '#fff',
    'borderColor': '#ddd',
    'cursor': 'not-allowed'
  },
  'pagination>disabled>span:focus': {
    'color': '#777',
    'backgroundColor': '#fff',
    'borderColor': '#ddd',
    'cursor': 'not-allowed'
  },
  'pagination>disabled>span:hover': {
    'color': '#777',
    'backgroundColor': '#fff',
    'borderColor': '#ddd',
    'cursor': 'not-allowed'
  },
  'pagination-lg>li>a': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }]
  },
  'pagination-lg>li>span': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 16 }],
    'fontSize': [{ 'unit': 'px', 'value': 18 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3333333 }]
  },
  'pagination-lg>li:first-child>a': {
    'borderBottomLeftRadius': '6px',
    'borderTopLeftRadius': '6px'
  },
  'pagination-lg>li:first-child>span': {
    'borderBottomLeftRadius': '6px',
    'borderTopLeftRadius': '6px'
  },
  'pagination-lg>li:last-child>a': {
    'borderBottomRightRadius': '6px',
    'borderTopRightRadius': '6px'
  },
  'pagination-lg>li:last-child>span': {
    'borderBottomRightRadius': '6px',
    'borderTopRightRadius': '6px'
  },
  'pagination-sm>li>a': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }]
  },
  'pagination-sm>li>span': {
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.5 }]
  },
  'badge': {
    'fontWeight': '700',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'whiteSpace': 'nowrap',
    'textAlign': 'center'
  },
  'label': {
    'fontWeight': '700',
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'whiteSpace': 'nowrap',
    'textAlign': 'center'
  },
  'pagination-sm>li:first-child>a': {
    'borderBottomLeftRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'pagination-sm>li:first-child>span': {
    'borderBottomLeftRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'pagination-sm>li:last-child>a': {
    'borderBottomRightRadius': '3px',
    'borderTopRightRadius': '3px'
  },
  'pagination-sm>li:last-child>span': {
    'borderBottomRightRadius': '3px',
    'borderTopRightRadius': '3px'
  },
  'pager': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 22 }, { 'unit': 'px', 'value': 0 }],
    'listStyle': 'none',
    'textAlign': 'center'
  },
  'pager:after': {
    'content': '" "',
    'display': 'table'
  },
  'pager:before': {
    'content': '" "',
    'display': 'table'
  },
  'pager li': {
    'display': 'inline'
  },
  'pager li>a': {
    'display': 'inline-block',
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 14 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderRadius': '15px'
  },
  'pager li>span': {
    'display': 'inline-block',
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 14 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderRadius': '15px'
  },
  'pager li>a:focus': {
    'textDecoration': 'none',
    'backgroundColor': '#eee'
  },
  'pager li>a:hover': {
    'textDecoration': 'none',
    'backgroundColor': '#eee'
  },
  'pager next>a': {
    'float': 'right'
  },
  'pager next>span': {
    'float': 'right'
  },
  'pager previous>a': {
    'float': 'left'
  },
  'pager previous>span': {
    'float': 'left'
  },
  'pager disabled>a': {
    'color': '#777',
    'backgroundColor': '#fff',
    'cursor': 'not-allowed'
  },
  'pager disabled>a:focus': {
    'color': '#777',
    'backgroundColor': '#fff',
    'cursor': 'not-allowed'
  },
  'pager disabled>a:hover': {
    'color': '#777',
    'backgroundColor': '#fff',
    'cursor': 'not-allowed'
  },
  'pager disabled>span': {
    'color': '#777',
    'backgroundColor': '#fff',
    'cursor': 'not-allowed'
  },
  'label': {
    'display': 'inline',
    'padding': [{ 'unit': 'em', 'value': 0.2 }, { 'unit': 'em', 'value': 0.6 }, { 'unit': 'em', 'value': 0.3 }, { 'unit': 'em', 'value': 0.6 }],
    'fontSize': [{ 'unit': '%V', 'value': 0.75 }],
    'color': '#fff',
    'borderRadius': '.25em'
  },
  'label:empty': {
    'display': 'none'
  },
  'alabel:focus': {
    'color': '#fff',
    'textDecoration': 'none',
    'cursor': 'pointer'
  },
  'alabel:hover': {
    'color': '#fff',
    'textDecoration': 'none',
    'cursor': 'pointer'
  },
  'label-default': {
    'backgroundColor': '#777'
  },
  'label-default[href]:focus': {
    'backgroundColor': '#5e5e5e'
  },
  'label-default[href]:hover': {
    'backgroundColor': '#5e5e5e'
  },
  'label-primary': {
    'backgroundColor': '#3097D1'
  },
  'label-primary[href]:focus': {
    'backgroundColor': '#2579a9'
  },
  'label-primary[href]:hover': {
    'backgroundColor': '#2579a9'
  },
  'label-success': {
    'backgroundColor': '#2ab27b'
  },
  'label-success[href]:focus': {
    'backgroundColor': '#20895e'
  },
  'label-success[href]:hover': {
    'backgroundColor': '#20895e'
  },
  'label-info': {
    'backgroundColor': '#8eb4cb'
  },
  'label-info[href]:focus': {
    'backgroundColor': '#6b9dbb'
  },
  'label-info[href]:hover': {
    'backgroundColor': '#6b9dbb'
  },
  'label-warning': {
    'backgroundColor': '#cbb956'
  },
  'label-warning[href]:focus': {
    'backgroundColor': '#b6a338'
  },
  'label-warning[href]:hover': {
    'backgroundColor': '#b6a338'
  },
  'label-danger': {
    'backgroundColor': '#bf5329'
  },
  'label-danger[href]:focus': {
    'backgroundColor': '#954120'
  },
  'label-danger[href]:hover': {
    'backgroundColor': '#954120'
  },
  'badge': {
    'display': 'inline-block',
    'minWidth': [{ 'unit': 'px', 'value': 10 }],
    'padding': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 7 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 7 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'color': '#fff',
    'verticalAlign': 'middle',
    'backgroundColor': '#777',
    'borderRadius': '10px'
  },
  'badge:empty': {
    'display': 'none'
  },
  'media-object': {
    'display': 'block'
  },
  'thumbnail': {
    'display': 'block'
  },
  'btn-group-xs>btn badge': {
    'top': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }]
  },
  'btn-xs badge': {
    'top': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 5 }]
  },
  'list-group-itemactive>badge': {
    'color': '#3097D1',
    'backgroundColor': '#fff'
  },
  'nav-pills>active>a>badge': {
    'color': '#3097D1',
    'backgroundColor': '#fff'
  },
  'list-group-item>badge': {
    'float': 'right'
  },
  'list-group-item>badge+badge': {
    'marginRight': [{ 'unit': 'px', 'value': 5 }]
  },
  'nav-pills>li>a>badge': {
    'marginLeft': [{ 'unit': 'px', 'value': 3 }]
  },
  'abadge:focus': {
    'color': '#fff',
    'textDecoration': 'none',
    'cursor': 'pointer'
  },
  'abadge:hover': {
    'color': '#fff',
    'textDecoration': 'none',
    'cursor': 'pointer'
  },
  'jumbotron': {
    'color': 'inherit',
    'screen&&>w768': {
      'paddingTop': [{ 'unit': 'px', 'value': 48 }],
      'paddingBottom': [{ 'unit': 'px', 'value': 48 }]
    }
  },
  'jumbotron h1': {
    'color': 'inherit',
    'screen&&>w768': {
      'paddingTop': [{ 'unit': 'px', 'value': 48 }],
      'paddingBottom': [{ 'unit': 'px', 'value': 48 }]
    }
  },
  'jumbotron h1': {
    'color': 'inherit',
    'screen&&>w768': {
      'paddingTop': [{ 'unit': 'px', 'value': 48 }],
      'paddingBottom': [{ 'unit': 'px', 'value': 48 }]
    }
  },
  'jumbotron': {
    'paddingTop': [{ 'unit': 'px', 'value': 30 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 30 }],
    'marginBottom': [{ 'unit': 'px', 'value': 30 }],
    'backgroundColor': '#eee'
  },
  'jumbotron p': {
    'marginBottom': [{ 'unit': 'px', 'value': 15 }],
    'fontSize': [{ 'unit': 'px', 'value': 21 }],
    'fontWeight': '200'
  },
  'alert': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }]
  },
  'thumbnail': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }]
  },
  'alert alert-link': {
    'fontWeight': '700'
  },
  'close': {
    'fontWeight': '700'
  },
  'jumbotron>hr': {
    'borderTopColor': '#d5d5d5'
  },
  'container jumbotron': {
    'borderRadius': '6px',
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'container-fluid jumbotron': {
    'borderRadius': '6px',
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'jumbotron container': {
    'maxWidth': [{ 'unit': '%H', 'value': 1 }]
  },
  'thumbnail': {
    'padding': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'backgroundColor': '#f5f8fa',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }],
    'borderRadius': '4px',
    'transition': 'border .2s ease-in-out'
  },
  'thumbnail a>img': {
    'display': 'block',
    'maxWidth': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'thumbnail>img': {
    'display': 'block',
    'maxWidth': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'thumbnail caption': {
    'padding': [{ 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 9 }],
    'color': '#636b6f'
  },
  'athumbnailactive': {
    'borderColor': '#3097D1'
  },
  'athumbnail:focus': {
    'borderColor': '#3097D1'
  },
  'athumbnail:hover': {
    'borderColor': '#3097D1'
  },
  'alert': {
    'padding': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderRadius': '4px'
  },
  'alert h4': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'color': 'inherit'
  },
  'alert>p': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'alert>ul': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'alert>p+p': {
    'marginTop': [{ 'unit': 'px', 'value': 5 }]
  },
  'alert-dismissable': {
    'paddingRight': [{ 'unit': 'px', 'value': 35 }]
  },
  'alert-dismissible': {
    'paddingRight': [{ 'unit': 'px', 'value': 35 }]
  },
  'alert-dismissable close': {
    'position': 'relative',
    'top': [{ 'unit': 'px', 'value': -2 }],
    'right': [{ 'unit': 'px', 'value': -21 }],
    'color': 'inherit'
  },
  'alert-dismissible close': {
    'position': 'relative',
    'top': [{ 'unit': 'px', 'value': -2 }],
    'right': [{ 'unit': 'px', 'value': -21 }],
    'color': 'inherit'
  },
  'modal': {
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'modal-backdrop': {
    'top': [{ 'unit': 'px', 'value': 0 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'alert-success': {
    'backgroundColor': '#dff0d8',
    'borderColor': '#d6e9c6',
    'color': '#3c763d'
  },
  'alert-success hr': {
    'borderTopColor': '#c9e2b3'
  },
  'alert-success alert-link': {
    'color': '#2b542c'
  },
  'alert-info': {
    'backgroundColor': '#d9edf7',
    'borderColor': '#bce8f1',
    'color': '#31708f'
  },
  'alert-info hr': {
    'borderTopColor': '#a6e1ec'
  },
  'alert-info alert-link': {
    'color': '#245269'
  },
  'alert-warning': {
    'backgroundColor': '#fcf8e3',
    'borderColor': '#faebcc',
    'color': '#8a6d3b'
  },
  'alert-warning hr': {
    'borderTopColor': '#f7e1b5'
  },
  'alert-warning alert-link': {
    'color': '#66512c'
  },
  'alert-danger': {
    'backgroundColor': '#f2dede',
    'borderColor': '#ebccd1',
    'color': '#a94442'
  },
  'alert-danger hr': {
    'borderTopColor': '#e4b9c0'
  },
  'alert-danger alert-link': {
    'color': '#843534'
  },
  'progress': {
    'height': [{ 'unit': 'px', 'value': 22 }],
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'backgroundColor': '#f5f5f5',
    'borderRadius': '4px',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.1)' }]
  },
  'progress-bar': {
    'float': 'left',
    'width': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'lineHeight': [{ 'unit': 'px', 'value': 22 }],
    'color': '#fff',
    'textAlign': 'center',
    'backgroundColor': '#3097D1',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': -1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.15)' }],
    'transition': 'width .6s ease'
  },
  'progress-bar-striped': {
    'backgroundSize': '40px 40px'
  },
  'progress-striped progress-bar': {
    'backgroundSize': '40px 40px'
  },
  'progress-baractive': {
    'WebkitAnimation': 'progress-bar-stripes 2s linear infinite',
    'animation': 'progress-bar-stripes 2s linear infinite'
  },
  'progressactive progress-bar': {
    'WebkitAnimation': 'progress-bar-stripes 2s linear infinite',
    'animation': 'progress-bar-stripes 2s linear infinite'
  },
  'progress-bar-success': {
    'backgroundColor': '#2ab27b'
  },
  'progress-bar-info': {
    'backgroundColor': '#8eb4cb'
  },
  'progress-bar-warning': {
    'backgroundColor': '#cbb956'
  },
  'progress-bar-danger': {
    'backgroundColor': '#bf5329'
  },
  'media': {
    'marginTop': [{ 'unit': 'px', 'value': 15 }]
  },
  'media:first-child': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'media': {
    'zoom': '1',
    'overflow': 'hidden'
  },
  'media-body': {
    'zoom': '1',
    'overflow': 'hidden'
  },
  'media-body': {
    'width': [{ 'unit': 'px', 'value': 10000 }]
  },
  'media-objectimg-thumbnail': {
    'maxWidth': [{ 'unit': 'string', 'value': 'none' }]
  },
  'media-right': {
    'paddingLeft': [{ 'unit': 'px', 'value': 10 }]
  },
  'media>pull-right': {
    'paddingLeft': [{ 'unit': 'px', 'value': 10 }]
  },
  'media-left': {
    'paddingRight': [{ 'unit': 'px', 'value': 10 }]
  },
  'media>pull-left': {
    'paddingRight': [{ 'unit': 'px', 'value': 10 }]
  },
  'media-body': {
    'display': 'table-cell',
    'verticalAlign': 'top'
  },
  'media-left': {
    'display': 'table-cell',
    'verticalAlign': 'top'
  },
  'media-right': {
    'display': 'table-cell',
    'verticalAlign': 'top'
  },
  'media-middle': {
    'verticalAlign': 'middle'
  },
  'media-bottom': {
    'verticalAlign': 'bottom'
  },
  'media-heading': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 5 }]
  },
  'media-list': {
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'listStyle': 'none'
  },
  'list-group': {
    'marginBottom': [{ 'unit': 'px', 'value': 20 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'list-group-item': {
    'position': 'relative',
    'display': 'block',
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }],
    'marginBottom': [{ 'unit': 'px', 'value': -1 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#d3e0e9' }]
  },
  'list-group-item:first-child': {
    'borderTopRightRadius': '4px',
    'borderTopLeftRadius': '4px'
  },
  'list-group-item:last-child': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomRightRadius': '4px',
    'borderBottomLeftRadius': '4px'
  },
  'alist-group-item': {
    'color': '#555'
  },
  'buttonlist-group-item': {
    'color': '#555'
  },
  'alist-group-item list-group-item-heading': {
    'color': '#333'
  },
  'buttonlist-group-item list-group-item-heading': {
    'color': '#333'
  },
  'alist-group-item:focus': {
    'textDecoration': 'none',
    'color': '#555',
    'backgroundColor': '#f5f5f5'
  },
  'alist-group-item:hover': {
    'textDecoration': 'none',
    'color': '#555',
    'backgroundColor': '#f5f5f5'
  },
  'buttonlist-group-item:focus': {
    'textDecoration': 'none',
    'color': '#555',
    'backgroundColor': '#f5f5f5'
  },
  'buttonlist-group-item:hover': {
    'textDecoration': 'none',
    'color': '#555',
    'backgroundColor': '#f5f5f5'
  },
  'buttonlist-group-item': {
    'width': [{ 'unit': '%H', 'value': 1 }],
    'textAlign': 'left'
  },
  'list-group-itemdisabled': {
    'backgroundColor': '#eee',
    'color': '#777',
    'cursor': 'not-allowed'
  },
  'list-group-itemdisabled:focus': {
    'backgroundColor': '#eee',
    'color': '#777',
    'cursor': 'not-allowed'
  },
  'list-group-itemdisabled:hover': {
    'backgroundColor': '#eee',
    'color': '#777',
    'cursor': 'not-allowed'
  },
  'list-group-itemdisabled list-group-item-heading': {
    'color': 'inherit'
  },
  'list-group-itemdisabled:focus list-group-item-heading': {
    'color': 'inherit'
  },
  'list-group-itemdisabled:hover list-group-item-heading': {
    'color': 'inherit'
  },
  'list-group-itemdisabled list-group-item-text': {
    'color': '#777'
  },
  'list-group-itemdisabled:focus list-group-item-text': {
    'color': '#777'
  },
  'list-group-itemdisabled:hover list-group-item-text': {
    'color': '#777'
  },
  'list-group-itemactive': {
    'zIndex': '2',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1'
  },
  'list-group-itemactive:focus': {
    'zIndex': '2',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1'
  },
  'list-group-itemactive:hover': {
    'zIndex': '2',
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1'
  },
  'list-group-itemactive list-group-item-heading': {
    'color': 'inherit'
  },
  'list-group-itemactive list-group-item-heading>small': {
    'color': 'inherit'
  },
  'list-group-itemactive list-group-item-heading>small': {
    'color': 'inherit'
  },
  'list-group-itemactive:focus list-group-item-heading': {
    'color': 'inherit'
  },
  'list-group-itemactive:focus list-group-item-heading>small': {
    'color': 'inherit'
  },
  'list-group-itemactive:focus list-group-item-heading>small': {
    'color': 'inherit'
  },
  'list-group-itemactive:hover list-group-item-heading': {
    'color': 'inherit'
  },
  'list-group-itemactive:hover list-group-item-heading>small': {
    'color': 'inherit'
  },
  'list-group-itemactive:hover list-group-item-heading>small': {
    'color': 'inherit'
  },
  'list-group-itemactive list-group-item-text': {
    'color': '#d7ebf6'
  },
  'list-group-itemactive:focus list-group-item-text': {
    'color': '#d7ebf6'
  },
  'list-group-itemactive:hover list-group-item-text': {
    'color': '#d7ebf6'
  },
  'list-group-item-success': {
    'color': '#3c763d',
    'backgroundColor': '#dff0d8'
  },
  'alist-group-item-success': {
    'color': '#3c763d'
  },
  'buttonlist-group-item-success': {
    'color': '#3c763d'
  },
  'alist-group-item-success list-group-item-heading': {
    'color': 'inherit'
  },
  'buttonlist-group-item-success list-group-item-heading': {
    'color': 'inherit'
  },
  'alist-group-item-success:focus': {
    'color': '#3c763d',
    'backgroundColor': '#d0e9c6'
  },
  'alist-group-item-success:hover': {
    'color': '#3c763d',
    'backgroundColor': '#d0e9c6'
  },
  'buttonlist-group-item-success:focus': {
    'color': '#3c763d',
    'backgroundColor': '#d0e9c6'
  },
  'buttonlist-group-item-success:hover': {
    'color': '#3c763d',
    'backgroundColor': '#d0e9c6'
  },
  'alist-group-item-successactive': {
    'color': '#fff',
    'backgroundColor': '#3c763d',
    'borderColor': '#3c763d'
  },
  'alist-group-item-successactive:focus': {
    'color': '#fff',
    'backgroundColor': '#3c763d',
    'borderColor': '#3c763d'
  },
  'alist-group-item-successactive:hover': {
    'color': '#fff',
    'backgroundColor': '#3c763d',
    'borderColor': '#3c763d'
  },
  'buttonlist-group-item-successactive': {
    'color': '#fff',
    'backgroundColor': '#3c763d',
    'borderColor': '#3c763d'
  },
  'buttonlist-group-item-successactive:focus': {
    'color': '#fff',
    'backgroundColor': '#3c763d',
    'borderColor': '#3c763d'
  },
  'buttonlist-group-item-successactive:hover': {
    'color': '#fff',
    'backgroundColor': '#3c763d',
    'borderColor': '#3c763d'
  },
  'list-group-item-info': {
    'color': '#31708f',
    'backgroundColor': '#d9edf7'
  },
  'alist-group-item-info': {
    'color': '#31708f'
  },
  'buttonlist-group-item-info': {
    'color': '#31708f'
  },
  'alist-group-item-info list-group-item-heading': {
    'color': 'inherit'
  },
  'buttonlist-group-item-info list-group-item-heading': {
    'color': 'inherit'
  },
  'alist-group-item-info:focus': {
    'color': '#31708f',
    'backgroundColor': '#c4e3f3'
  },
  'alist-group-item-info:hover': {
    'color': '#31708f',
    'backgroundColor': '#c4e3f3'
  },
  'buttonlist-group-item-info:focus': {
    'color': '#31708f',
    'backgroundColor': '#c4e3f3'
  },
  'buttonlist-group-item-info:hover': {
    'color': '#31708f',
    'backgroundColor': '#c4e3f3'
  },
  'alist-group-item-infoactive': {
    'color': '#fff',
    'backgroundColor': '#31708f',
    'borderColor': '#31708f'
  },
  'alist-group-item-infoactive:focus': {
    'color': '#fff',
    'backgroundColor': '#31708f',
    'borderColor': '#31708f'
  },
  'alist-group-item-infoactive:hover': {
    'color': '#fff',
    'backgroundColor': '#31708f',
    'borderColor': '#31708f'
  },
  'buttonlist-group-item-infoactive': {
    'color': '#fff',
    'backgroundColor': '#31708f',
    'borderColor': '#31708f'
  },
  'buttonlist-group-item-infoactive:focus': {
    'color': '#fff',
    'backgroundColor': '#31708f',
    'borderColor': '#31708f'
  },
  'buttonlist-group-item-infoactive:hover': {
    'color': '#fff',
    'backgroundColor': '#31708f',
    'borderColor': '#31708f'
  },
  'list-group-item-warning': {
    'color': '#8a6d3b',
    'backgroundColor': '#fcf8e3'
  },
  'alist-group-item-warning': {
    'color': '#8a6d3b'
  },
  'buttonlist-group-item-warning': {
    'color': '#8a6d3b'
  },
  'alist-group-item-warning list-group-item-heading': {
    'color': 'inherit'
  },
  'buttonlist-group-item-warning list-group-item-heading': {
    'color': 'inherit'
  },
  'alist-group-item-warning:focus': {
    'color': '#8a6d3b',
    'backgroundColor': '#faf2cc'
  },
  'alist-group-item-warning:hover': {
    'color': '#8a6d3b',
    'backgroundColor': '#faf2cc'
  },
  'buttonlist-group-item-warning:focus': {
    'color': '#8a6d3b',
    'backgroundColor': '#faf2cc'
  },
  'buttonlist-group-item-warning:hover': {
    'color': '#8a6d3b',
    'backgroundColor': '#faf2cc'
  },
  'alist-group-item-warningactive': {
    'color': '#fff',
    'backgroundColor': '#8a6d3b',
    'borderColor': '#8a6d3b'
  },
  'alist-group-item-warningactive:focus': {
    'color': '#fff',
    'backgroundColor': '#8a6d3b',
    'borderColor': '#8a6d3b'
  },
  'alist-group-item-warningactive:hover': {
    'color': '#fff',
    'backgroundColor': '#8a6d3b',
    'borderColor': '#8a6d3b'
  },
  'buttonlist-group-item-warningactive': {
    'color': '#fff',
    'backgroundColor': '#8a6d3b',
    'borderColor': '#8a6d3b'
  },
  'buttonlist-group-item-warningactive:focus': {
    'color': '#fff',
    'backgroundColor': '#8a6d3b',
    'borderColor': '#8a6d3b'
  },
  'buttonlist-group-item-warningactive:hover': {
    'color': '#fff',
    'backgroundColor': '#8a6d3b',
    'borderColor': '#8a6d3b'
  },
  'list-group-item-danger': {
    'color': '#a94442',
    'backgroundColor': '#f2dede'
  },
  'alist-group-item-danger': {
    'color': '#a94442'
  },
  'buttonlist-group-item-danger': {
    'color': '#a94442'
  },
  'alist-group-item-danger list-group-item-heading': {
    'color': 'inherit'
  },
  'buttonlist-group-item-danger list-group-item-heading': {
    'color': 'inherit'
  },
  'alist-group-item-danger:focus': {
    'color': '#a94442',
    'backgroundColor': '#ebcccc'
  },
  'alist-group-item-danger:hover': {
    'color': '#a94442',
    'backgroundColor': '#ebcccc'
  },
  'buttonlist-group-item-danger:focus': {
    'color': '#a94442',
    'backgroundColor': '#ebcccc'
  },
  'buttonlist-group-item-danger:hover': {
    'color': '#a94442',
    'backgroundColor': '#ebcccc'
  },
  'alist-group-item-dangeractive': {
    'color': '#fff',
    'backgroundColor': '#a94442',
    'borderColor': '#a94442'
  },
  'alist-group-item-dangeractive:focus': {
    'color': '#fff',
    'backgroundColor': '#a94442',
    'borderColor': '#a94442'
  },
  'alist-group-item-dangeractive:hover': {
    'color': '#fff',
    'backgroundColor': '#a94442',
    'borderColor': '#a94442'
  },
  'buttonlist-group-item-dangeractive': {
    'color': '#fff',
    'backgroundColor': '#a94442',
    'borderColor': '#a94442'
  },
  'buttonlist-group-item-dangeractive:focus': {
    'color': '#fff',
    'backgroundColor': '#a94442',
    'borderColor': '#a94442'
  },
  'buttonlist-group-item-dangeractive:hover': {
    'color': '#fff',
    'backgroundColor': '#a94442',
    'borderColor': '#a94442'
  },
  'panel-heading>dropdown dropdown-toggle': {
    'color': 'inherit'
  },
  'panel-title': {
    'color': 'inherit'
  },
  'panel-title>small': {
    'color': 'inherit'
  },
  'panel-title>small>a': {
    'color': 'inherit'
  },
  'panel-title>a': {
    'color': 'inherit'
  },
  'panel-title>small': {
    'color': 'inherit'
  },
  'panel-title>small>a': {
    'color': 'inherit'
  },
  'list-group-item-heading': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 5 }]
  },
  'list-group-item-text': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.3 }]
  },
  'panel': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderRadius': '4px',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.05)' }]
  },
  'panel-title': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>list-group': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>panel-collapse>list-group': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>panel-collapse>table': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel-body': {
    'padding': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }]
  },
  'panel-body:after': {
    'content': '" "',
    'display': 'table'
  },
  'panel-body:before': {
    'content': '" "',
    'display': 'table'
  },
  'panel-heading': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'transparent' }],
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel-title': {
    'marginTop': [{ 'unit': 'px', 'value': 0 }],
    'fontSize': [{ 'unit': 'px', 'value': 16 }]
  },
  'panel-footer': {
    'padding': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 15 }],
    'backgroundColor': '#f5f5f5',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#d3e0e9' }],
    'borderBottomRightRadius': '3px',
    'borderBottomLeftRadius': '3px'
  },
  'panel>list-group list-group-item': {
    'borderWidth': '1px 0',
    'borderRadius': '0'
  },
  'panel>panel-collapse>list-group list-group-item': {
    'borderWidth': '1px 0',
    'borderRadius': '0'
  },
  'panel-group panel-heading': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr:first-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr:first-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr:last-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr:last-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tfoot>tr:last-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tfoot>tr:last-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>thead>tr:first-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>thead>tr:first-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr:first-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr:first-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr:last-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr:last-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tfoot>tr:last-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tfoot>tr:last-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>thead>tr:first-child>td': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>thead>tr:first-child>th': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive:last-child>table:last-child': {
    'borderBottomLeftRadius': '3px',
    'borderBottomRightRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tbody:last-child>tr:last-child': {
    'borderBottomLeftRadius': '3px',
    'borderBottomRightRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tfoot:last-child>tr:last-child': {
    'borderBottomLeftRadius': '3px',
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child': {
    'borderBottomLeftRadius': '3px',
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child>tbody:last-child>tr:last-child': {
    'borderBottomLeftRadius': '3px',
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child>tfoot:last-child>tr:last-child': {
    'borderBottomLeftRadius': '3px',
    'borderBottomRightRadius': '3px'
  },
  'panel>list-group:first-child list-group-item:first-child': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }],
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>panel-collapse>list-group:first-child list-group-item:first-child': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }],
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>list-group:last-child list-group-item:last-child': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomRightRadius': '3px',
    'borderBottomLeftRadius': '3px'
  },
  'panel>panel-collapse>list-group:last-child list-group-item:last-child': {
    'borderBottom': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomRightRadius': '3px',
    'borderBottomLeftRadius': '3px'
  },
  'panel>panel-heading+panel-collapse>list-group list-group-item:first-child': {
    'borderTopRightRadius': '0',
    'borderTopLeftRadius': '0'
  },
  'panel>table-responsive:first-child>table:first-child': {
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>tbody:first-child>tr:first-child': {
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>thead:first-child>tr:first-child': {
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child': {
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child>tbody:first-child>tr:first-child': {
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child>thead:first-child>tr:first-child': {
    'borderTopRightRadius': '3px',
    'borderTopLeftRadius': '3px'
  },
  'list-group+panel-footer': {
    'borderTopWidth': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel-heading+list-group list-group-item:first-child': {
    'borderTopWidth': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>panel-collapse>table caption': {
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'panel>table caption': {
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'panel>table-responsive>table caption': {
    'paddingLeft': [{ 'unit': 'px', 'value': 15 }],
    'paddingRight': [{ 'unit': 'px', 'value': 15 }]
  },
  'panel>table-responsive:first-child>table:first-child>tbody:first-child>tr:first-child td:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>tbody:first-child>tr:first-child th:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>thead:first-child>tr:first-child td:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>thead:first-child>tr:first-child th:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child>tbody:first-child>tr:first-child td:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child>tbody:first-child>tr:first-child th:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child>thead:first-child>tr:first-child td:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table:first-child>thead:first-child>tr:first-child th:first-child': {
    'borderTopLeftRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>tbody:first-child>tr:first-child td:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>tbody:first-child>tr:first-child th:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>thead:first-child>tr:first-child td:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table-responsive:first-child>table:first-child>thead:first-child>tr:first-child th:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table:first-child>tbody:first-child>tr:first-child td:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table:first-child>tbody:first-child>tr:first-child th:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table:first-child>thead:first-child>tr:first-child td:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table:first-child>thead:first-child>tr:first-child th:last-child': {
    'borderTopRightRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tbody:last-child>tr:last-child td:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tbody:last-child>tr:last-child th:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tfoot:last-child>tr:last-child td:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tfoot:last-child>tr:last-child th:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table:last-child>tbody:last-child>tr:last-child td:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table:last-child>tbody:last-child>tr:last-child th:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table:last-child>tfoot:last-child>tr:last-child td:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table:last-child>tfoot:last-child>tr:last-child th:first-child': {
    'borderBottomLeftRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tbody:last-child>tr:last-child td:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tbody:last-child>tr:last-child th:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tfoot:last-child>tr:last-child td:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table-responsive:last-child>table:last-child>tfoot:last-child>tr:last-child th:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child>tbody:last-child>tr:last-child td:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child>tbody:last-child>tr:last-child th:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child>tfoot:last-child>tr:last-child td:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>table:last-child>tfoot:last-child>tr:last-child th:last-child': {
    'borderBottomRightRadius': '3px'
  },
  'panel>panel-body+table': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'panel>panel-body+table-responsive': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'panel>table+panel-body': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'panel>table-responsive+panel-body': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ddd' }]
  },
  'panel>table>tbody:first-child>tr:first-child td': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table>tbody:first-child>tr:first-child th': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered': {
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered': {
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr>td:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr>th:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tfoot>tr>td:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tfoot>tr>th:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>thead>tr>td:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>thead>tr>th:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr>td:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr>th:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tfoot>tr>td:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tfoot>tr>th:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>thead>tr>td:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>thead>tr>th:first-child': {
    'borderLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr>td:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tbody>tr>th:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tfoot>tr>td:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>tfoot>tr>th:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>thead>tr>td:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-bordered>thead>tr>th:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr>td:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tbody>tr>th:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tfoot>tr>td:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>tfoot>tr>th:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>thead>tr>td:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive>table-bordered>thead>tr>th:last-child': {
    'borderRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel>table-responsive': {
    'border': [{ 'unit': 'px', 'value': 0 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel-group': {
    'marginBottom': [{ 'unit': 'px', 'value': 22 }]
  },
  'panel-group panel': {
    'marginBottom': [{ 'unit': 'px', 'value': 0 }],
    'borderRadius': '4px'
  },
  'panel-group panel+panel': {
    'marginTop': [{ 'unit': 'px', 'value': 5 }]
  },
  'panel-group panel-heading+panel-collapse>list-group': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#d3e0e9' }]
  },
  'panel-group panel-heading+panel-collapse>panel-body': {
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#d3e0e9' }]
  },
  'panel-group panel-footer': {
    'borderTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'panel-group panel-footer+panel-collapse panel-body': {
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#d3e0e9' }]
  },
  'panel-default': {
    'borderColor': '#d3e0e9'
  },
  'panel-default>panel-heading': {
    'color': '#333',
    'backgroundColor': '#fff',
    'borderColor': '#d3e0e9'
  },
  'panel-default>panel-heading+panel-collapse>panel-body': {
    'borderTopColor': '#d3e0e9'
  },
  'panel-default>panel-heading badge': {
    'color': '#fff',
    'backgroundColor': '#333'
  },
  'panel-default>panel-footer+panel-collapse>panel-body': {
    'borderBottomColor': '#d3e0e9'
  },
  'panel-primary': {
    'borderColor': '#3097D1'
  },
  'panel-primary>panel-heading': {
    'color': '#fff',
    'backgroundColor': '#3097D1',
    'borderColor': '#3097D1'
  },
  'panel-primary>panel-heading+panel-collapse>panel-body': {
    'borderTopColor': '#3097D1'
  },
  'panel-primary>panel-heading badge': {
    'color': '#3097D1',
    'backgroundColor': '#fff'
  },
  'panel-primary>panel-footer+panel-collapse>panel-body': {
    'borderBottomColor': '#3097D1'
  },
  'panel-success': {
    'borderColor': '#d6e9c6'
  },
  'panel-success>panel-heading': {
    'color': '#3c763d',
    'backgroundColor': '#dff0d8',
    'borderColor': '#d6e9c6'
  },
  'panel-success>panel-heading+panel-collapse>panel-body': {
    'borderTopColor': '#d6e9c6'
  },
  'panel-success>panel-heading badge': {
    'color': '#dff0d8',
    'backgroundColor': '#3c763d'
  },
  'panel-success>panel-footer+panel-collapse>panel-body': {
    'borderBottomColor': '#d6e9c6'
  },
  'panel-info': {
    'borderColor': '#bce8f1'
  },
  'panel-info>panel-heading': {
    'color': '#31708f',
    'backgroundColor': '#d9edf7',
    'borderColor': '#bce8f1'
  },
  'panel-info>panel-heading+panel-collapse>panel-body': {
    'borderTopColor': '#bce8f1'
  },
  'panel-info>panel-heading badge': {
    'color': '#d9edf7',
    'backgroundColor': '#31708f'
  },
  'panel-info>panel-footer+panel-collapse>panel-body': {
    'borderBottomColor': '#bce8f1'
  },
  'panel-warning': {
    'borderColor': '#faebcc'
  },
  'panel-warning>panel-heading': {
    'color': '#8a6d3b',
    'backgroundColor': '#fcf8e3',
    'borderColor': '#faebcc'
  },
  'panel-warning>panel-heading+panel-collapse>panel-body': {
    'borderTopColor': '#faebcc'
  },
  'panel-warning>panel-heading badge': {
    'color': '#fcf8e3',
    'backgroundColor': '#8a6d3b'
  },
  'panel-warning>panel-footer+panel-collapse>panel-body': {
    'borderBottomColor': '#faebcc'
  },
  'panel-danger': {
    'borderColor': '#ebccd1'
  },
  'panel-danger>panel-heading': {
    'color': '#a94442',
    'backgroundColor': '#f2dede',
    'borderColor': '#ebccd1'
  },
  'panel-danger>panel-heading+panel-collapse>panel-body': {
    'borderTopColor': '#ebccd1'
  },
  'panel-danger>panel-heading badge': {
    'color': '#f2dede',
    'backgroundColor': '#a94442'
  },
  'panel-danger>panel-footer+panel-collapse>panel-body': {
    'borderBottomColor': '#ebccd1'
  },
  'embed-responsive': {
    'position': 'relative',
    'display': 'block',
    'height': [{ 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'embed-responsive embed-responsive-item': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'embed-responsive embed': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'embed-responsive iframe': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'embed-responsive object': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'embed-responsive video': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': '%V', 'value': 1 }],
    'width': [{ 'unit': '%H', 'value': 1 }],
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'embed-responsive-16by9': {
    'paddingBottom': [{ 'unit': '%V', 'value': 0.5625 }]
  },
  'embed-responsive-4by3': {
    'paddingBottom': [{ 'unit': '%V', 'value': 0.75 }]
  },
  'well': {
    'minHeight': [{ 'unit': 'px', 'value': 20 }],
    'padding': [{ 'unit': 'px', 'value': 19 }, { 'unit': 'px', 'value': 19 }, { 'unit': 'px', 'value': 19 }, { 'unit': 'px', 'value': 19 }],
    'marginBottom': [{ 'unit': 'px', 'value': 20 }],
    'backgroundColor': '#f5f5f5',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#e3e3e3' }],
    'borderRadius': '4px',
    'boxShadow': [{ 'unit': 'string', 'value': 'inset' }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.05)' }]
  },
  'well blockquote': {
    'borderColor': '#ddd',
    'borderColor': 'rgba(0,0,0,.15)'
  },
  'well-lg': {
    'padding': [{ 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 24 }, { 'unit': 'px', 'value': 24 }],
    'borderRadius': '6px'
  },
  'well-sm': {
    'padding': [{ 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 9 }],
    'borderRadius': '3px'
  },
  'close': {
    'float': 'right',
    'fontSize': [{ 'unit': 'px', 'value': 21 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'color': '#000',
    'textShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': '#fff' }],
    'opacity': '.2',
    'filter': 'alpha(opacity=20)'
  },
  'popover': {
    'fontFamily': 'Raleway,sans-serif',
    'fontStyle': 'normal',
    'fontWeight': '400',
    'letterSpacing': [{ 'unit': 'string', 'value': 'normal' }],
    'lineBreak': 'auto',
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'textTransform': 'none',
    'whiteSpace': 'normal',
    'wordBreak': 'normal',
    'wordSpacing': 'normal',
    'wordWrap': 'normal',
    'textDecoration': 'none'
  },
  'tooltip': {
    'fontFamily': 'Raleway,sans-serif',
    'fontStyle': 'normal',
    'fontWeight': '400',
    'letterSpacing': [{ 'unit': 'string', 'value': 'normal' }],
    'lineBreak': 'auto',
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }],
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }],
    'textTransform': 'none',
    'whiteSpace': 'normal',
    'wordBreak': 'normal',
    'wordSpacing': 'normal',
    'wordWrap': 'normal',
    'textDecoration': 'none'
  },
  'close:focus': {
    'color': '#000',
    'textDecoration': 'none',
    'cursor': 'pointer',
    'opacity': '.5',
    'filter': 'alpha(opacity=50)'
  },
  'close:hover': {
    'color': '#000',
    'textDecoration': 'none',
    'cursor': 'pointer',
    'opacity': '.5',
    'filter': 'alpha(opacity=50)'
  },
  'buttonclose': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'cursor': 'pointer',
    'background': '0 0',
    'border': [{ 'unit': 'px', 'value': 0 }],
    'WebkitAppearance': 'none'
  },
  'modal-content': {
    'backgroundClip': 'padding-box'
  },
  'popover': {
    'backgroundClip': 'padding-box'
  },
  'modal': {
    'display': 'none',
    'position': 'fixed',
    'zIndex': '1050',
    'WebkitOverflowScrolling': 'touch',
    'outline': '0'
  },
  'modal-footer:after': {
    'display': 'table',
    'content': '" "'
  },
  'modal-footer:before': {
    'display': 'table',
    'content': '" "'
  },
  'modal-header:after': {
    'display': 'table',
    'content': '" "'
  },
  'modal-header:before': {
    'display': 'table',
    'content': '" "'
  },
  'modalfade modal-dialog': {
    'WebkitTransform': 'translate(0,-25%)',
    'transform': 'translate(0,-25%)',
    'transition': '-webkit-transform .3s ease-out',
    'transition': 'transform .3s ease-out',
    'transition': 'transform .3s ease-out,-webkit-transform .3s ease-out'
  },
  'modalin modal-dialog': {
    'WebkitTransform': 'translate(0,0)',
    'transform': 'translate(0,0)'
  },
  'modal-open modal': {
    'overflowX': 'hidden',
    'overflowY': 'auto'
  },
  'modal-dialog': {
    'position': 'relative',
    'width': [{ 'unit': 'string', 'value': 'auto' }],
    'margin': [{ 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'px', 'value': 10 }],
    '>w768': {
      'width': [{ 'unit': 'px', 'value': 600 }],
      'margin': [{ 'unit': 'px', 'value': 30 }, { 'unit': 'string', 'value': 'auto' }, { 'unit': 'px', 'value': 30 }, { 'unit': 'string', 'value': 'auto' }]
    }
  },
  'modal-content': {
    'position': 'relative',
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#999' }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.2)' }],
    'borderRadius': '6px',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.5)' }],
    'outline': '0'
  },
  'modal-backdrop': {
    'position': 'fixed',
    'zIndex': '1040',
    'backgroundColor': '#000'
  },
  'modal-backdropfade': {
    'opacity': '0',
    'filter': 'alpha(opacity=0)'
  },
  'modal-backdropin': {
    'opacity': '.5',
    'filter': 'alpha(opacity=50)'
  },
  'modal-header': {
    'padding': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }],
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#e5e5e5' }]
  },
  'modal-header close': {
    'marginTop': [{ 'unit': 'px', 'value': -2 }]
  },
  'modal-title': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1.6 }]
  },
  'modal-body': {
    'position': 'relative',
    'padding': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }]
  },
  'modal-footer': {
    'padding': [{ 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 15 }],
    'textAlign': 'right',
    'borderTop': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#e5e5e5' }]
  },
  'modal-footer btn+btn': {
    'marginLeft': [{ 'unit': 'px', 'value': 5 }],
    'marginBottom': [{ 'unit': 'px', 'value': 0 }]
  },
  'modal-footer btn-group btn+btn': {
    'marginLeft': [{ 'unit': 'px', 'value': -1 }]
  },
  'modal-footer btn-block+btn-block': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }]
  },
  'modal-scrollbar-measure': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': -9999 }],
    'width': [{ 'unit': 'px', 'value': 50 }],
    'height': [{ 'unit': 'px', 'value': 50 }],
    'overflow': 'scroll'
  },
  'tooltip': {
    'position': 'absolute',
    'zIndex': '1070',
    'display': 'block',
    'textAlign': 'left',
    'textAlign': 'start',
    'fontSize': [{ 'unit': 'px', 'value': 12 }],
    'opacity': '0',
    'filter': 'alpha(opacity=0)'
  },
  'tooltipin': {
    'opacity': '.9',
    'filter': 'alpha(opacity=90)'
  },
  'tooltiptop': {
    'marginTop': [{ 'unit': 'px', 'value': -3 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }]
  },
  'tooltipright': {
    'marginLeft': [{ 'unit': 'px', 'value': 3 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }]
  },
  'tooltipbottom': {
    'marginTop': [{ 'unit': 'px', 'value': 3 }],
    'padding': [{ 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }]
  },
  'tooltipleft': {
    'marginLeft': [{ 'unit': 'px', 'value': -3 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }]
  },
  'tooltip-inner': {
    'maxWidth': [{ 'unit': 'px', 'value': 200 }],
    'padding': [{ 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 3 }, { 'unit': 'px', 'value': 8 }],
    'color': '#fff',
    'textAlign': 'center',
    'backgroundColor': '#000',
    'borderRadius': '4px'
  },
  'tooltip-arrow': {
    'position': 'absolute',
    'width': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 0 }],
    'borderColor': 'transparent',
    'borderStyle': 'solid'
  },
  'tooltiptop tooltip-arrow': {
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'borderWidth': '5px 5px 0',
    'borderTopColor': '#000'
  },
  'tooltiptop-left tooltip-arrow': {
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'borderWidth': '5px 5px 0',
    'borderTopColor': '#000'
  },
  'tooltiptop-right tooltip-arrow': {
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'borderWidth': '5px 5px 0',
    'borderTopColor': '#000'
  },
  'tooltiptop tooltip-arrow': {
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -5 }]
  },
  'tooltiptop-left tooltip-arrow': {
    'right': [{ 'unit': 'px', 'value': 5 }],
    'marginBottom': [{ 'unit': 'px', 'value': -5 }]
  },
  'tooltiptop-right tooltip-arrow': {
    'left': [{ 'unit': 'px', 'value': 5 }],
    'marginBottom': [{ 'unit': 'px', 'value': -5 }]
  },
  'tooltipright tooltip-arrow': {
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'marginTop': [{ 'unit': 'px', 'value': -5 }],
    'borderWidth': '5px 5px 5px 0',
    'borderRightColor': '#000'
  },
  'tooltipleft tooltip-arrow': {
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'marginTop': [{ 'unit': 'px', 'value': -5 }],
    'borderWidth': '5px 0 5px 5px',
    'borderLeftColor': '#000'
  },
  'tooltipbottom tooltip-arrow': {
    'borderWidth': '0 5px 5px',
    'borderBottomColor': '#000',
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'tooltipbottom-left tooltip-arrow': {
    'borderWidth': '0 5px 5px',
    'borderBottomColor': '#000',
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'tooltipbottom-right tooltip-arrow': {
    'borderWidth': '0 5px 5px',
    'borderBottomColor': '#000',
    'top': [{ 'unit': 'px', 'value': 0 }]
  },
  'tooltipbottom tooltip-arrow': {
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -5 }]
  },
  'tooltipbottom-left tooltip-arrow': {
    'right': [{ 'unit': 'px', 'value': 5 }],
    'marginTop': [{ 'unit': 'px', 'value': -5 }]
  },
  'tooltipbottom-right tooltip-arrow': {
    'left': [{ 'unit': 'px', 'value': 5 }],
    'marginTop': [{ 'unit': 'px', 'value': -5 }]
  },
  'popover': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'zIndex': '1060',
    'display': 'none',
    'maxWidth': [{ 'unit': 'px', 'value': 276 }],
    'padding': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }],
    'textAlign': 'left',
    'textAlign': 'start',
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'backgroundColor': '#fff',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ccc' }],
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': 'rgba(0,0,0,.2)' }],
    'borderRadius': '6px',
    'boxShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 5 }, { 'unit': 'px', 'value': 10 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.2)' }]
  },
  'carousel-caption': {
    'color': '#fff',
    'textAlign': 'center',
    'textShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.6)' }]
  },
  'carousel-control': {
    'color': '#fff',
    'textAlign': 'center',
    'textShadow': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'string', 'value': 'rgba(0,0,0,.6)' }]
  },
  'popovertop': {
    'marginTop': [{ 'unit': 'px', 'value': -10 }]
  },
  'popoverright': {
    'marginLeft': [{ 'unit': 'px', 'value': 10 }]
  },
  'popoverbottom': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }]
  },
  'popoverleft': {
    'marginLeft': [{ 'unit': 'px', 'value': -10 }]
  },
  'popover-title': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'padding': [{ 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 8 }, { 'unit': 'px', 'value': 14 }],
    'fontSize': [{ 'unit': 'px', 'value': 14 }],
    'backgroundColor': '#f7f7f7',
    'borderBottom': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#ebebeb' }],
    'borderRadius': '5px 5px 0 0'
  },
  'popover-content': {
    'padding': [{ 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 14 }, { 'unit': 'px', 'value': 9 }, { 'unit': 'px', 'value': 14 }]
  },
  'popover>arrow': {
    'position': 'absolute',
    'display': 'block',
    'width': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 0 }],
    'borderColor': 'transparent',
    'borderStyle': 'solid'
  },
  'popover>arrow:after': {
    'position': 'absolute',
    'display': 'block',
    'width': [{ 'unit': 'px', 'value': 0 }],
    'height': [{ 'unit': 'px', 'value': 0 }],
    'borderColor': 'transparent',
    'borderStyle': 'solid'
  },
  'carousel': {
    'position': 'relative'
  },
  'carousel-inner': {
    'position': 'relative'
  },
  'popover>arrow': {
    'borderWidth': '11px'
  },
  'popover>arrow:after': {
    'borderWidth': '10px',
    'content': '""'
  },
  'popovertop>arrow': {
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -11 }],
    'borderBottomWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderTopColor': '#999',
    'borderTopColor': 'rgba(0,0,0,.25)',
    'bottom': [{ 'unit': 'px', 'value': -11 }]
  },
  'popovertop>arrow:after': {
    'content': '" "',
    'bottom': [{ 'unit': 'px', 'value': 1 }],
    'marginLeft': [{ 'unit': 'px', 'value': -10 }],
    'borderBottomWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderTopColor': '#fff'
  },
  'popoverleft>arrow:after': {
    'content': '" "',
    'bottom': [{ 'unit': 'px', 'value': -10 }]
  },
  'popoverright>arrow:after': {
    'content': '" "',
    'bottom': [{ 'unit': 'px', 'value': -10 }]
  },
  'popoverright>arrow': {
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'left': [{ 'unit': 'px', 'value': -11 }],
    'marginTop': [{ 'unit': 'px', 'value': -11 }],
    'borderLeftWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderRightColor': '#999',
    'borderRightColor': 'rgba(0,0,0,.25)'
  },
  'popoverright>arrow:after': {
    'left': [{ 'unit': 'px', 'value': 1 }],
    'borderLeftWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderRightColor': '#fff'
  },
  'popoverbottom>arrow': {
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -11 }],
    'borderTopWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomColor': '#999',
    'borderBottomColor': 'rgba(0,0,0,.25)',
    'top': [{ 'unit': 'px', 'value': -11 }]
  },
  'popoverbottom>arrow:after': {
    'content': '" "',
    'top': [{ 'unit': 'px', 'value': 1 }],
    'marginLeft': [{ 'unit': 'px', 'value': -10 }],
    'borderTopWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderBottomColor': '#fff'
  },
  'popoverleft>arrow': {
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'right': [{ 'unit': 'px', 'value': -11 }],
    'marginTop': [{ 'unit': 'px', 'value': -11 }],
    'borderRightWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderLeftColor': '#999',
    'borderLeftColor': 'rgba(0,0,0,.25)'
  },
  'popoverleft>arrow:after': {
    'right': [{ 'unit': 'px', 'value': 1 }],
    'borderRightWidth': [{ 'unit': 'px', 'value': 0 }],
    'borderLeftColor': '#fff'
  },
  'carousel-inner': {
    'overflow': 'hidden',
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'carousel-inner>item': {
    'display': 'none',
    'position': 'relative',
    'transition': '.6s ease-in-out left',
    'all&&transform-3||-webkit-transform-3': {
      'transition': '-webkit-transform .6s ease-in-out',
      'transition': 'transform .6s ease-in-out',
      'transition': 'transform .6s ease-in-out,-webkit-transform .6s ease-in-out',
      'WebkitBackfaceVisibility': 'hidden',
      'backfaceVisibility': 'hidden',
      'WebkitPerspective': '1000px',
      'perspective': '1000px'
    }
  },
  'carousel-inner>item>a>img': {
    'display': 'block',
    'maxWidth': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }]
  },
  'carousel-inner>item>img': {
    'display': 'block',
    'maxWidth': [{ 'unit': '%H', 'value': 1 }],
    'height': [{ 'unit': 'string', 'value': 'auto' }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }]
  },
  'carousel-inner>active': {
    'display': 'block'
  },
  'carousel-inner>next': {
    'display': 'block'
  },
  'carousel-inner>prev': {
    'display': 'block'
  },
  'carousel-inner>active': {
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'carousel-inner>next': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'carousel-inner>prev': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'carousel-inner>next': {
    'left': [{ 'unit': '%H', 'value': 1 }]
  },
  'carousel-inner>prev': {
    'left': [{ 'unit': '%H', 'value': -1 }]
  },
  'carousel-inner>nextleft': {
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'carousel-inner>prevright': {
    'left': [{ 'unit': 'px', 'value': 0 }]
  },
  'carousel-inner>activeleft': {
    'left': [{ 'unit': '%H', 'value': -1 }]
  },
  'carousel-inner>activeright': {
    'left': [{ 'unit': '%H', 'value': 1 }]
  },
  'carousel-control': {
    'position': 'absolute',
    'top': [{ 'unit': 'px', 'value': 0 }],
    'left': [{ 'unit': 'px', 'value': 0 }],
    'bottom': [{ 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': '%H', 'value': 0.15 }],
    'opacity': '.5',
    'filter': 'alpha(opacity=50)',
    'fontSize': [{ 'unit': 'px', 'value': 20 }],
    'backgroundColor': 'transparent'
  },
  'carousel-controlleft': {
    'backgroundImage': 'linear-gradient(to right,rgba(0,0,0,.5) 0,rgba(0,0,0,.0001) 100%)',
    'backgroundRepeat': 'repeat-x',
    'filter': 'progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1)'
  },
  'carousel-controlright': {
    'left': [{ 'unit': 'string', 'value': 'auto' }],
    'right': [{ 'unit': 'px', 'value': 0 }],
    'backgroundImage': 'linear-gradient(to right,rgba(0,0,0,.0001) 0,rgba(0,0,0,.5) 100%)',
    'backgroundRepeat': 'repeat-x',
    'filter': 'progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1)'
  },
  'carousel-control:focus': {
    'outline': '0',
    'color': '#fff',
    'textDecoration': 'none',
    'opacity': '.9',
    'filter': 'alpha(opacity=90)'
  },
  'carousel-control:hover': {
    'outline': '0',
    'color': '#fff',
    'textDecoration': 'none',
    'opacity': '.9',
    'filter': 'alpha(opacity=90)'
  },
  'carousel-control glyphicon-chevron-left': {
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'marginTop': [{ 'unit': 'px', 'value': -10 }],
    'zIndex': '5',
    'display': 'inline-block',
    'screen&&>w768': {
      'width': [{ 'unit': 'px', 'value': 30 }],
      'height': [{ 'unit': 'px', 'value': 30 }],
      'marginTop': [{ 'unit': 'px', 'value': -10 }],
      'fontSize': [{ 'unit': 'px', 'value': 30 }]
    }
  },
  'carousel-control glyphicon-chevron-right': {
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'marginTop': [{ 'unit': 'px', 'value': -10 }],
    'zIndex': '5',
    'display': 'inline-block',
    'screen&&>w768': {
      'width': [{ 'unit': 'px', 'value': 30 }],
      'height': [{ 'unit': 'px', 'value': 30 }],
      'marginTop': [{ 'unit': 'px', 'value': -10 }],
      'fontSize': [{ 'unit': 'px', 'value': 30 }]
    }
  },
  'carousel-control icon-next': {
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'marginTop': [{ 'unit': 'px', 'value': -10 }],
    'zIndex': '5',
    'display': 'inline-block',
    'screen&&>w768': {
      'width': [{ 'unit': 'px', 'value': 30 }],
      'height': [{ 'unit': 'px', 'value': 30 }],
      'marginTop': [{ 'unit': 'px', 'value': -10 }],
      'fontSize': [{ 'unit': 'px', 'value': 30 }]
    }
  },
  'carousel-control icon-prev': {
    'position': 'absolute',
    'top': [{ 'unit': '%V', 'value': 0.5 }],
    'marginTop': [{ 'unit': 'px', 'value': -10 }],
    'zIndex': '5',
    'display': 'inline-block',
    'screen&&>w768': {
      'width': [{ 'unit': 'px', 'value': 30 }],
      'height': [{ 'unit': 'px', 'value': 30 }],
      'marginTop': [{ 'unit': 'px', 'value': -10 }],
      'fontSize': [{ 'unit': 'px', 'value': 30 }]
    }
  },
  'carousel-control glyphicon-chevron-left': {
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -10 }]
  },
  'carousel-control icon-prev': {
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'marginLeft': [{ 'unit': 'px', 'value': -10 }]
  },
  'carousel-control glyphicon-chevron-right': {
    'right': [{ 'unit': '%H', 'value': 0.5 }],
    'marginRight': [{ 'unit': 'px', 'value': -10 }]
  },
  'carousel-control icon-next': {
    'right': [{ 'unit': '%H', 'value': 0.5 }],
    'marginRight': [{ 'unit': 'px', 'value': -10 }]
  },
  'carousel-control icon-next': {
    'width': [{ 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'px', 'value': 20 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': 'serif'
  },
  'carousel-control icon-prev': {
    'width': [{ 'unit': 'px', 'value': 20 }],
    'height': [{ 'unit': 'px', 'value': 20 }],
    'lineHeight': [{ 'unit': 'px', 'value': 1 }],
    'fontFamily': 'serif'
  },
  'carousel-control icon-prev:before': {
    'content': ''\2039''
  },
  'carousel-control icon-next:before': {
    'content': ''\203a''
  },
  'carousel-indicators': {
    'position': 'absolute',
    'bottom': [{ 'unit': 'px', 'value': 10 }],
    'left': [{ 'unit': '%H', 'value': 0.5 }],
    'zIndex': '15',
    'width': [{ 'unit': '%H', 'value': 0.6 }],
    'marginLeft': [{ 'unit': '%H', 'value': -0.3 }],
    'paddingLeft': [{ 'unit': 'px', 'value': 0 }],
    'listStyle': 'none',
    'textAlign': 'center'
  },
  'carousel-indicators li': {
    'display': 'inline-block',
    'width': [{ 'unit': 'px', 'value': 10 }],
    'height': [{ 'unit': 'px', 'value': 10 }],
    'margin': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 1 }],
    'textIndent': '-999px',
    'border': [{ 'unit': 'px', 'value': 1 }, { 'unit': 'string', 'value': 'solid' }, { 'unit': 'string', 'value': '#fff' }],
    'borderRadius': '10px',
    'cursor': 'pointer',
    'backgroundColor': '#000\9',
    'backgroundColor': 'transparent'
  },
  'carousel-indicators active': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'width': [{ 'unit': 'px', 'value': 12 }],
    'height': [{ 'unit': 'px', 'value': 12 }],
    'backgroundColor': '#fff'
  },
  'carousel-caption': {
    'position': 'absolute',
    'left': [{ 'unit': '%H', 'value': 0.15 }],
    'right': [{ 'unit': '%H', 'value': 0.15 }],
    'bottom': [{ 'unit': 'px', 'value': 20 }],
    'zIndex': '10',
    'paddingTop': [{ 'unit': 'px', 'value': 20 }],
    'paddingBottom': [{ 'unit': 'px', 'value': 20 }]
  },
  'carousel-caption btn': {
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'text-hide': {
    'textShadow': [{ 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }, { 'unit': 'string', 'value': 'none' }]
  },
  'clearfix:after': {
    'content': '" "',
    'display': 'table'
  },
  'clearfix:before': {
    'content': '" "',
    'display': 'table'
  },
  'center-block': {
    'display': 'block',
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'pull-right': {
    'float': 'right!important'
  },
  'pull-left': {
    'float': 'left!important'
  },
  'hide': {
    'display': 'none!important'
  },
  'show': {
    'display': 'block!important'
  },
  'hidden': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-lg': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-lg-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-lg-inline': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-lg-inline-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-md': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-md-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-md-inline': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-md-inline-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-sm': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-sm-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-sm-inline': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-sm-inline-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-xs': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-xs-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-xs-inline': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'visible-xs-inline-block': {
    'display': 'none!important',
    '<w767': {
      'display': 'block!important'
    },
    '>w768&&<w991': {
      'display': 'block!important'
    },
    '>w992&&<w1199': {
      'display': 'block!important'
    },
    '>w1200': {
      'display': 'block!important'
    }
  },
  'invisible': {
    'visibility': 'hidden'
  },
  'text-hide': {
    'font': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'string', 'value': 'a' }],
    'color': 'transparent',
    'backgroundColor': 'transparent',
    'border': [{ 'unit': 'px', 'value': 0 }]
  },
  'affix': {
    'position': 'fixed'
  },
  '@-ms-viewport': {
    'width': [{ 'unit': 'string', 'value': 'device-width' }]
  },
  'visible-print': {
    'display': 'none!important',
    '': {
      'display': 'block!important'
    }
  },
  'visible-print-block': {
    'display': 'none!important',
    '': {
      'display': 'block!important'
    }
  },
  'visible-print-inline': {
    'display': 'none!important',
    '': {
      'display': 'inline!important'
    }
  },
  'visible-print-inline-block': {
    'display': 'none!important',
    '': {
      'display': 'inline-block!important'
    }
  }
});
