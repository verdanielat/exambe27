<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/mycontroller/{name}', 'MyController@greetings');


// Route::get('/hello', 'MyController@hello'); // from hello.blade.php (materi view)

Route::get('/hello', 'MyController@hello');
Route::get('/content/child1', 'MyController@child1');

Route::get('/child1',function(){


$user = [
    ["name" => "A", "Address" => "Address 1", "PhoneNumber"=> "082115386755"],
    ["name" => "B", "Address" => "Address 2", "PhoneNumber"=> "082115386685"],
    ["name" => "C", "Address" => "Address 3", "PhoneNumber"=> "082115345445"],
];
    return view('content.child1',["userList" => $user]);

});

Route::get('/child2',function(){
    return view('content.child2');
});





// Route::get('/hello',function(){
//     return view('hello',['name' => ' verdaniel']);
// });


Route::get('/home', function () {
    return view('welcome');
});


Route::get('/verdaniel', function () {
    return "hellooo world ";
});


Route::get('/user/{id}/{name}', function ($id, $name = "Hello") { //{id} adalah parameter
    return " User Id :" .  $id . "<br> My Name : " . $name ;
})->where(['id' => '[A-Z]{3}']);
