<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



    Route::group(['prefix' => 'products'], function(){ 
    Route::get('/','ProductController@getUnit');
    Route::post('/create','ProductController@createUnit');
    Route::post('/delete','ProducttController@deleteUnit');
});

    Route::group(['prefix' => 'customers'], function(){ 
    Route::get('/','CustomerController@index');
    Route::post('/add','CustomerController@addCustomer');
    Route::post('/delete','CustomerController@deleteCustomer');
});
    
        













